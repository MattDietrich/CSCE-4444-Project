/**************************************************************
- Create and populate tables.
**************************************************************/
CREATE TABLE SystemUser (
    ID int NOT NULL,
    Username varchar(25) NOT NULL,
    Password varchar(25) NOT NULL,

    PRIMARY KEY (ID)
);

CREATE TABLE StaffCategory (
    ID int NOT NULL,
    Name varchar(50),
    PRIMARY KEY (ID)
);

CREATE TABLE MenuItemCategory (
    ID int NOT NULL,
    Name varchar (50),

    PRIMARY KEY (ID)
);

CREATE TABLE OrderStatusCategory (
    ID int NOT NULL,
    Name varchar(50),

    PRIMARY KEY (ID)
);

CREATE TABLE CouponType (
    ID int NOT NULL,
    Name varchar(50),
    PRIMARY KEY (ID)
);
CREATE TABLE PaymentType (
    ID int NOT NULL,
    Name varchar(50),
    PRIMARY KEY (ID)
);

CREATE TABLE RestaurantTable (
    ID int NOT NULL,
    Name varchar(50),
    NumOfCustomers int,
    NeedsRefill bit,
    NeedsHelp bit,
    PRIMARY KEY (ID)
);

CREATE TABLE StaffMember (
    ID int NOT NULL,
    FirstName varchar(50),
    LastName varchar(50),
    CategoryID int,
    UserID int NOT NULL,

    PRIMARY KEY (ID),
    FOREIGN KEY (CategoryID) REFERENCES StaffCategory(ID),
    FOREIGN KEY (UserID) REFERENCES SystemUser(ID)
);

CREATE TABLE Customer (
    ID int NOT NULL,
    FirstName varchar(50),
    LastName varchar(50),
    UserID int,

    PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES SystemUser(ID)
);

CREATE TABLE RestaurantOrder (
    ID int NOT NULL AUTO_INCREMENT,
    FromTableID int NOT NULL,
    AssignedStaffID int NOT NULL,
    Status int NOT NULL,
    PayType int NOT NULL,
    Price float,
    Comments TEXT,
    Allergies TEXT,

    PRIMARY KEY (ID),
    FOREIGN KEY (PayType) REFERENCES PaymentType(ID),
    FOREIGN KEY (FromTableID) REFERENCES RestaurantTable(ID),
    FOREIGN KEY (AssignedStaffID) REFERENCES StaffMember(ID),
    FOREIGN KEY (Status) REFERENCES OrderStatusCategory(ID)
);

CREATE TABLE MenuItem (
    ID int NOT NULL,
    Name varchar(50),
    Price float,
    CategoryID int,
    ImageURL TEXT,
    PRIMARY KEY (ID),
    FOREIGN KEY (CategoryID) REFERENCES MenuItemCategory(ID)
);

CREATE TABLE OrderItem (
    ID int NOT NULL AUTO_INCREMENT,
    OrderID int NOT NULL,
    ItemID int NOT NULL,
    PayType int NOT NULL,
    Paid bool NOT NULL,

    PRIMARY KEY (ID),
    FOREIGN KEY (PayType) REFERENCES PaymentType(ID),
    FOREIGN KEY (OrderID) REFERENCES RestaurantOrder(ID),
    FOREIGN KEY (ItemID) REFERENCES MenuItem(ID)
);

CREATE TABLE UserSession (
    ID int NOT NULL,
    UserID int NOT NULL,

    PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES SystemUser(ID)
);

CREATE TABLE UserSession (
    ID int NOT NULL,
    UserID int NOT NULL,

    PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES SystemUser(ID)
);

CREATE TABLE Coupons (
    ID int NOT NULL,
    TypeID int NOT NULL,
    Code int NOT NULL,
    ExpDay int,
    ExpMonth int,
    ExpYear int,
    PRIMARY KEY (ID),
    FOREIGN KEY (TypeID) REFERENCES CouponType(ID)
);


INSERT INTO StaffCategory (ID, Name) VALUES (1, 'Waiter');
INSERT INTO StaffCategory (ID, Name) VALUES (2, 'Kitchen');
INSERT INTO StaffCategory (ID, Name) VALUES (3, 'Manager');

INSERT INTO MenuItemCategory (ID, Name) VALUES (1, 'Entree');
INSERT INTO MenuItemCategory (ID, Name) VALUES (2, 'Appetizer');
INSERT INTO MenuItemCategory (ID, Name) VALUES (3, 'Side dish');
INSERT INTO MenuItemCategory (ID, Name) VALUES (4, 'Dessert');
INSERT INTO MenuItemCategory (ID, Name) VALUES (5, 'Kid\'s meal');
INSERT INTO MenuItemCategory (ID, Name) VALUES (6, 'Beverage');
INSERT INTO MenuItemCategory (ID, Name) VALUES (7, 'Merchandise');

INSERT INTO OrderStatusCategory (ID, Name) VALUES (1, 'Received by kitchen');
INSERT INTO OrderStatusCategory (ID, Name) VALUES (2, 'Cooking');
INSERT INTO OrderStatusCategory (ID, Name) VALUES (3, 'On the way');
INSERT INTO OrderStatusCategory (ID, Name) VALUES (4, 'Delivered');

INSERT INTO CouponType (ID,Name) VALUES (1, 'Free Dessert');

INSERT INTO PaymentType (ID, Name) VALUES (1, 'Unpaid');
INSERT INTO PaymentType (ID, Name) VALUES (2, 'Cash');
INSERT INTO PaymentType (ID, Name) VALUES (3, 'Card');

/* Tables in restaurant */
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (1, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (2, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (3, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (4, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (5, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (6, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (7, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (8, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (9, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (10, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (11, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (12, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (13, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (14, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (15, 0, false, false);
INSERT INTO RestaurantTable (ID, NumOfCustomers, NeedsRefill, NeedsHelp) VALUES (16, 0, false, false);

/* Coupon types */
INSERT INTO CouponType (ID, Name) VALUES (1, "DESSERTGAME");
INSERT INTO CouponType (ID, Name) VALUES (2, "COMPITEM");
INSERT INTO CouponType (ID, Name) VALUES (3, "BOGOENTREE");
