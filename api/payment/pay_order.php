<?php

include_once "../config/api_setup.php";
include_once "../config/database.php";

header("Access-Control-Allow-Methods: POST");

$result = array();
$result['ok'] = false;
$result['result'] = null;
$result['error'] = null;


// immediately die if not POST

if ($_SERVER['REQUEST_METHOD'] !== "POST") {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: POST");

    $result['error'] = "Method not allowed: " . $_SERVER['REQUEST_METHOD'];
    echo json_encode($result);
    die();
}


// required: order_id, payment_type
if (!isset($_POST['order_id']) || !isset($_POST['payment_type'])) {
    header("HTTP/1.1 400 Bad Request");

    $result['error'] = "Required: order_id, payment_type";
    echo json_encode($result);
    die();
}


// connect to db

$db = new Database();
$conn = $db->getConnection();


// set order's PayType to match. returns true iff success, else false
function set_order_paid($order_id, $payment_type) {
    global $conn, $result;

    $order_paid_query  = "UPDATE RestaurantOrder SET PayType = " . $payment_type;
    $order_paid_query .= ", Status = 6 WHERE ID = " . $order_id;

    $order_paid_stmt = $conn->prepare($order_paid_query);

    if (!$order_paid_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Unable to update order PayType: database error";
        return false;
    }

    return true;
}


// set each OrderItem to paid = true. returns true iff success, else false
function set_items_paid($order_id) {
    global $conn, $result;

    $items_paid_query = "UPDATE OrderItem SET Paid = TRUE WHERE OrderID = " . $order_id;
    $items_paid_stmt = $conn->prepare($items_paid_query);

    if (!$items_paid_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Unable to update OrderItems: database error";
        return false;
    }

    return true;
}



// get POST data

$order_id = $_POST['order_id'];
$payment_type = $_POST['payment_type'];


// attempt to set order paid and items paid; bail out if either fails
$order_ok = set_order_paid($order_id, $payment_type);

if ($order_ok) {
    $items_ok = set_items_paid($order_id);

    if ($items_ok) {
        $result['ok'] = true;
    }
}

echo json_encode($result);
die();

?>
