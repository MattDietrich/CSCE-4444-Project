<?php

include_once "../config/api_setup.php";
include_once "../config/database.php";
include_once "../auth/sessions.php";

header("Access-Control-Allow-Methods: POST");

$result = array();
$result['ok'] = false;
$result['result'] = null;
$result['error'] = null;


// immediately die if not POST
if ($_SERVER['REQUEST_METHOD'] !== "POST") {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: POST");
    $result['error'] = "Method not allowed: " . $_SERVER['REQUEST_METHOD'];
    echo json_encode($result);
    die();
}


// required: token
if (!isset($_POST['token']) || !validate_token($_POST['token'])) {
    header("HTTP/1.1 403 Forbidden");
    $result['error'] = "Bad or missing token";
    echo json_encode($result);
    die();
}

// required: order_id, payment_type
if (!isset($_POST['items'])) {
    header("HTTP/1.1 400 Bad Request");
    $result['error'] = "Required: items, payment_type";
    echo json_encode($result);
    die();
}


// connect to db
$db = new Database();
$conn = $db->getConnection();


// set each OrderItem to paid = true. returns true iff success, else false
function set_items_paid($items) {
    global $conn, $result;

    $items_paid_query  = "UPDATE OrderItem SET Paid = TRUE, Comped = TRUE WHERE ID IN (" . join(", ", $items) . ")";
    $items_paid_stmt = $conn->prepare($items_paid_query);

    if (!$items_paid_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Unable to update OrderItems: database error";
        return false;
    }

    return true;
}



// get POST data

$items = $_POST['items'];

// attempt to set order paid and items paid; bail out if either fails
if (!set_items_paid($items)) {
    echo json_encode($result);
    die();
}


// must be OK
$result['ok'] = true;
echo json_encode($result);
die();

?>
