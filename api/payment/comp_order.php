<?php

include_once "../config/api_setup.php";
include_once "../config/database.php";

header("Access-Control-Allow-Methods: POST");

$result = array();
$result['ok'] = false;
$result['result'] = null;
$result['error'] = null;


// immediately die if not POST

if ($_SERVER['REQUEST_METHOD'] !== "POST") {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: POST");

    $result['error'] = "Method not allowed: " . $_SERVER['REQUEST_METHOD'];
    echo json_encode($result);
    die();
}


// required: order_id, payment_type
if (!isset($_POST['order_id'])) {
    header("HTTP/1.1 400 Bad Request");

    $result['error'] = "Required: order_id";
    echo json_encode($result);
    die();
}


// connect to db

$db = new Database();
$conn = $db->getConnection();


// set order's PayType to match. returns true iff success, else false
function set_order_paid($order_id) {
    global $conn, $result;

    $order_paid_query  = "UPDATE RestaurantOrder SET PayType = 4";
    $order_paid_query .= ", Status = 6 WHERE ID = " . $order_id;

    $order_paid_stmt = $conn->prepare($order_paid_query);

    if (!$order_paid_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Unable to update order PayType: database error";
        return false;
    }

    return true;
}


// set each OrderItem to paid = true. returns true iff success, else false
function set_items_paid($order_id) {
    global $conn, $result;

    $items_paid_query = "UPDATE OrderItem SET Paid = TRUE, Comped = TRUE WHERE OrderID = " . $order_id;
    $items_paid_stmt = $conn->prepare($items_paid_query);

    if (!$items_paid_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Unable to update OrderItems: database error";
        return false;
    }

    return true;
}


// mark order as comped
function mark_order_comped($order_id) {
    global $conn, $result;

    $staff_stmt = $conn->prepare("SELECT UserID FROM UserSession WHERE ID = :token");
    $staff_stmt->bindValue(':token', $_COOKIE['todo_internal_token'], PDO::PARAM_INT);

    if (!$staff_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        $error = $staff_stmt->errorInfo();
        $result['error'] = "Unable to fetch User ID: $error[2] ($error[1])";
        return false;
    }

    $staff_id = $staff_stmt->fetch(PDO::FETCH_ASSOC)['UserID'];

    $comped_stmt = $conn->prepare("INSERT INTO CompedOrders (OrderID, StaffID) VALUES (:orderID, :staffID)");
    $comped_stmt->bindValue(':orderID', $order_id, PDO::PARAM_INT);
    $comped_stmt->bindValue(':staffID', $staff_id, PDO::PARAM_INT);

    if (!$comped_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        $error = $comped_stmt->errorInfo();
        $result['error'] = "Unable to mark order as comped: $error[2] ($error[1])";
        return false;
    }

    return true;
}



// get POST data

$order_id = $_POST['order_id'];

$order_ok = set_items_paid($order_id) && set_order_paid($order_id) && mark_order_comped($order_id);
$result['ok'] = $order_ok;

echo json_encode($result);
die();

?>
