<!DOCTYPE html>
<html>
    <head>
        <title>Add Order &ndash; #TODO Eats</title>
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
        <script type="text/javascript">
            $(() => {
                $('#order-form').submit(() => {

                    var payload = {
                        order_id: $('#order-id input').val(),
                        status: $('#new-status input').val()
                    };

                    var request = $.ajax({
                        type: "POST",
                        url: "order_update.php",
                        data: payload,
                        success: res => {
                            var order_id = $('#order-id input').val();
                            var new_status = res['result']['new_status'];

                            $('#order-success-id').text(order_id);
                            $('#order-success-status').text(new_status);
                            $('#order-success').show();
                        },
                        dataTyle: 'json'
                    });

                    request.always(res => {
                        console.log(JSON.stringify(res, null, 4));
                    });

                    return false;

                });
            });
        </script>
    </head>
    <body role="document">
        <h1>#TODO - Landing page</h1>
        <form id="order-form" action="login.php" method="post">
            <div class="container" id="order-id">
                <label for="order_id"><b>Order</b></label>
                <input type="text" placeholder="Order ID" name="order_id" required>
            </div>
            <div class="container" id="new-status">
                <label for="new_status"><b>New Status</b></label>
                <input type="text" placeholder="New Status" name="new_status" required>
            </div>
            <div class="container" id="order-submit">
                <button type="submit">Update</button>
            </div>
        </form>
        <div id="order-success" style="display:none;">
            <h6>Successfully updated order <span id="order-success-id"></span> with status <span id="order-success-status"></span>!</h6>
        </div>
    </body>
</html>
