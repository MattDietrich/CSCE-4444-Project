<?php
/*  NOTES
Input: 
(category) = return all items in that category 
(id) = return specific food item
() = return all food items
*/


include_once "config/api_setup.php";
include_once "config/database.php";

header("Access-Control-Allow-Methods: GET");

 // establish database connection
    $db = new Database();
    $conn = $db->getConnection();
 // result and variables
    $result = array();
    $result['ok'] = false;
    $result['result'] = null;
    $result['error'] = null;


//Read every row from MenuItem and return
function parse_all() {
    global $conn, $result;

    $parse_result = array();

    $query = "SELECT * FROM MenuItem";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    
    $num = $stmt->rowCount();
	
    if ($num < 1) {
        $result['error'] = "No items in this category";
        return $parse_result;
    }

    /* fetch categories */

    $cats_query = "SELECT * FROM MenuItemCategory";
    $cats_stmt = $conn->prepare($cats_query);
    $cats_stmt->execute();

    $categories = array();
    while ($cats_row = $cats_stmt->fetch(PDO::FETCH_ASSOC)) {
        $categories[$cats_row['ID']] = $cats_row['Name'];
    }

    /* fetch menu items */

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $category_id = $row['CategoryID'];
        $category_name = $categories[$category_id];

        $row['CategoryName'] = $category_name;

        // create category array if it doesn't exist 
        if (!isset($parse_result[$category_id])) {
            $parse_result[$category_id] = array();
            $parse_result[$category_id]['CategoryID'] = $category_id;
            $parse_result[$category_id]['CategoryName'] = $category_name;
            $parse_result[$category_id]['Items'] = array();
        }

        // add this item to the category array
        $index = count($parse_result[$category_id]['Items']);
        $parse_result[$category_id]['Items'][$index] = $row;
    }

    $result['ok'] = true;
	$result['result'] = $parse_result;
	return $result;
	
}

//Read only items in certain category, used for populating menu for customer order process
function parse_category($category) {
    global $conn, $result;
    $query = "SELECT * FROM MenuItem WHERE CategoryID = '" . $category . "'";
    $stmt = $conn->prepare($query);
    $stmt->execute();
	
    $num = $stmt->rowCount();
	
    if ($num < 1) {
        $result['error'] = "No such category in menu";
    }
    else {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
		$result['result'] = array();
        $result['result'][0] = $row;
        $result['ok'] = true;
    }

    return $result;
}

//Read only one specific food item
function parse_item($id) {
    global $conn, $result;

    $query = "SELECT * FROM MenuItem WHERE ID = '" . $id . "'";
    $stmt = $conn->prepare($query);
    $stmt->execute();
	
    $num = $stmt->rowCount();
	
    if ($num < 1) {
        // no such item
        $result['error'] = "No such item in the menu";
    }
    else {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $result['result'] = array();
        $result['result'][0] = $row;
        $result['ok'] = true;
    }

    return $result;
}

//Only using get
if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: GET");

    $result['error'] = "Method not allowed";
    echo(json_encode($result));
    die();
}

//searching for item, category or all  
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $result = parse_item($id);
} else if(isset($_GET['category'])) {
    $category = $_GET['category'];
    $result = parse_category($category);
} else {
    $result = parse_all();
}

echo json_encode($result);

?>
