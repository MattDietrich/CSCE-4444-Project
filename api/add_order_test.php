<!DOCTYPE html>
<html>
    <head>
        <title>Add Order &ndash; #TODO Eats</title>
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
        <script type="text/javascript">
            $(() => {
                $('#order-form').submit(() => {

                    var payload = {
                        table_id: $('#order-table-id input').val(),
                        items: [{ ID: 1 }]
                    };

                    var request = $.ajax({
                        type: "POST",
                        url: "order_edited.php",
                        data: payload,
                        success: res => {
                            var order_id = res['result']['order_id'];
                            $('#order-success-number').text(order_id);
                            $('#order-success').show();
                        },
                        dataTyle: 'json'
                    });

                    request.always(res => {
                        console.log(JSON.stringify(res, null, 4));
                    });

                    return false;

                });
            });
        </script>
    </head>
    <body role="document">
        <h1>#TODO - Landing page</h1>
        <form id="order-form" action="login.php" method="post">
            <div class="container" id="order-table-id">
                <label for="username"><b>Table</b></label>
                <input type="text" placeholder="Table ID" name="table_id" required>
            </div>
            <div class="container" id="order-submit">
                <button type="submit">Login</button>
            </div>
        </form>
        <div id="order-success" style="display:none;">
            <h6>Successfully placed order <span id="order-success-number"></span>!</h6>
        </div>
    </body>
</html>
