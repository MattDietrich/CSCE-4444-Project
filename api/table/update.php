<?php

include_once "../config/api_setup.php";
include_once "../config/database.php";
include_once "../auth/sessions.php";

header("Access-Control-Allow-Methods: POST");

$db = new Database();
$conn = $db->getConnection();

$result = array();
$result['ok'] = false;
$result['error'] = null;
$result['result'] = null;

/* i fucking hate the if-statement in this one but time crunch - shawn */
function update_status($table_id) {
    global $conn, $result;

        try {
            $conn->beginTransaction();

            if (isset($_POST['needs_refill'])) {
                $needs_refill = filter_var($_POST['needs_refill'], FILTER_VALIDATE_BOOLEAN);
                $needs_refill = ($needs_refill ? "1" : "0");

                $query = "UPDATE RestaurantTable SET NeedsRefill = " . $needs_refill . " WHERE ID = " . $table_id;
                $conn->exec($query);
            }

            if (isset($_POST['needs_help'])) {
                $needs_help = filter_var($_POST['needs_help'], FILTER_VALIDATE_BOOLEAN);
                $needs_help = ($needs_help ? "1" : "0");

                $query = "UPDATE RestaurantTable SET NeedsHelp = " . $needs_help . " WHERE ID = " . $table_id;
                $conn->exec($query);
            }

            $conn->commit();
            $result['ok'] = true;
        } catch (Exception $e) {
            $conn->rollBack();
            header("HTTP/1.1 500 Internal Server Error");
            $result['error'] = "Error processing transaction";
            return;
    }
}



if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    /* neither is specified */
    if (!isset($_POST['table_id']) || (!isset($_POST['needs_refill']) && !isset($_POST['needs_help']))) {
        header("HTTP/1.1 400 Bad Request");
        $result['error'] = "Required: table_id AND (needs_refill AND/OR needs_help)";
    }

    else {
        update_status($_POST['table_id']);
    }
}

else {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: POST");
    $result['error'] = "Method not allowed";
}

echo json_encode($result);
die();

?>
