<?php

# HTTP response headers and anything that EVERY endpoint will do
include_once '../config/api_setup.php';

# TODO: CHANGE TO 'GET, PUT' WHENEVER THOSE METHODS ARE SUPPORTED
# I.E. GET returns table or list of tables,
#      PUT updates the status of a table
header("Access-Control-Allow-Methods: GET");


# Include any necessary files
# In this case, we need the Table class defined there
include_once '../objects/table.php';


/**
 * Produce an associative array (i.e. dictionary) from the row out of the database.
 *
 * @author Shawn Lutch
 */
function parse_row($row) {
    extract($row);

    return array(
        "id" => $ID,
        "num_customers" => $NumOfCustomers,
        "needs_refill" => $NeedsRefill,
        "needs_help" => $NeedsHelp
    );
}


/**
 * Read all rows from RestaurantTable table and return a response.
 *
 * @author Shawn Lutch
 */
function read_all() {
    $table = new Table();
    $stmt = $table->read_all();
    $num = $stmt->rowCount();

    if ($num == 0) {
        return array("ok" => false, "message" => "No restaurant tables.");
    } else {
        $tables_arr = array();

        $tables_arr["ok"] = true;
        $tables_arr["tables"] = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this_table = parse_row($row);
            array_push($tables_arr["tables"], $this_table);
        }

        return $tables_arr;
    }
}


/**
 * Request a specific row from RestaurantTable, i.e. the one with the given ID,
 * and return a response.
 *
 * @author Shawn Lutch
 */
function read_specific($id) {
    $table = new Table();
    $stmt = $table->read_by_id($id);
    $num = $stmt->rowCount();

    if ($num == 0) {
        return array("ok" => false, "message" => "No table with id '" . $id . "'");
    }

    $result = array();
    $result["ok"] = true;

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $this_table = parse_row($row);
    $result["table"] = $this_table;

    return $result;
}


/* HERE'S WHERE THE ACTUAL STUFF HAPPENS */


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Process GET request

    $result = null;

    if (array_key_exists('id', $_GET)) {

        // the client has requested a specific id
        $result = read_specific($_GET['id']);

    } else {

        // no id has been specified, i.e. client wants ALL table statuses
        $result = read_all();

    }

    // write the result (in JSON form) to the response.
    // json_encode() is fuckin NICE, yall
    echo json_encode($result);
}


// TODO: handle PUT requests, to update table status

?>
