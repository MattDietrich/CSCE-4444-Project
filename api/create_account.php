<?php
include_once "config/api_setup.php";
include_once "config/database.php";

header("Access-Control-Allow-Methods: POST");

$result = array();
$result['ok'] = false;
$result['error'] = null;
$result['result'] = null;

$database = new Database();
$conn = $database->getConnection();

function username_exists($username) {
    global $conn;
    $query = "SELECT Username FROM SystemUser WHERE Username = " . $username . ";";
    $stmt = $conn->prepare($query);
    $stmt->execute();

    if ($stmt->rowCount() > 0) return true;
    else return false;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!isset($_POST['username']) || !isset($_POST['password'])) {
        header("HTTP/1.1 400 Bad Request");
        $result['error'] = "Required: username, password";
    }

    // Check that username doesn't already exist
    else if (username_exists($_POST['username'])) {
        header("HTTP/1.1 403 Forbidden");
        $result['error'] = "Username already exists";
    }

    else {
        // Get number of rows to determine ID number of new user
        $query = "SELECT * FROM SystemUser;";
        $stmt = $conn->prepare($query);
        $stmt->execute();

        $queryArray = array($stmt->rowCount() + 1, "'" . $_POST['username'] . "'", "'" . $_POST['password'] . "'");
        $query = "INSERT INTO SystemUser (ID, Username, Password) VALUES (" . implode(", ", $queryArray) . ");";
        $stmt = $conn->prepare($query);
        $stmt->execute();

        $result['ok'] = true;
        $result['result'] = "User added";
    }
}

else {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: GET, POST, PUT");
    $result['error'] = "Method not allowed";
}

echo json_encode($result);
?>
