<?php

/**
 * Anything that EVERY endpoint will need to do
 *
 * Currently: declare origin and content type headers
 * Anything else?
 */

// allow from any origin
header("Access-Control-Allow-Origin: *");

// declare that we will be serving JSON, in the UTF-8 charset
header("Content-Type: application/json; charset=UTF-8");

?>
