<!DOCTYPE html>
<html>
    <head>
        <title>Landing Page &ndash; #TODO Eats</title>
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
        <script type="text/javascript">
            $(() => {
                $('#login-form').submit(() => {

                    var payload = {
                        username: $('#login-username input').val(),
                        password: $('#login-password input').val()
                    };

                    $.post("login.php", payload, res => {
                        var token = res['result']['token'];
                        Cookies.set('todo_session_id', token, { expires: 30 });
                        console.log(Cookies.get('todo_session_id'));
                        $('#login-success').show();
                    }, 'json');

                    return false;

                });
            });
        </script>
    </head>
    <body role="document">
        <h1>#TODO - Landing page</h1>
        <form id="login-form" action="login.php" method="post">
            <div class="container" id="login-username">
                <label for="username"><b>Username</b></label>
                <input type="text" placeholder="Staff login" name="username" required>
            </div>
            <div class="container" id="login-password">
                <label for="password"><b>Password</b></label>
                <input type="password" placeholder="Staff password" name="password" required>
            </div>
            <div class="container" id="login-submit">
                <button type="submit">Login</button>
            </div>
        </form>
        <div id="login-success" style="display:none;">
            <h6>Login success!</h6>
        </div>
    </body>
</html>
