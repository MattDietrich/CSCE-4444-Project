<?php

/*
Added info by Brayden: take out any if not necessary
-added if statement to determine request
-worked on completing get functions
-added an add order: not implemented
-added an update order: not impelmeted

reader warning: worked on in middle of the night(oops) so who knows 

*/


include_once "config/api_setup.php";
include_once "config/database.php";

header("Access-Control-Allow-Methods: GET, POST, PUT");

// result default values
$result = array();
$result['ok'] = false;
$result['result'] = null;
$result['error'] = null;

// database connection
$db = new Database();
$conn = $db->getConnection();


function get_order_items($order_id) {
    global $conn, $result;

    $get_result = array();
    $get_result['order_id'] = $order_id;
    $get_result['total_price'] = 0;
    $get_result['items'] = array();

    $order_status_query = "SELECT * FROM RestaurantOrder WHERE ID = " . $order_id;
    $order_status_stmt = $conn->prepare($order_status_query);

    if (!$order_status_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Unable to query for order status";
        return null;
    }

    if ($order_status_stmt->rowCount() < 1) {
        $result['error'] = "No such order";
        return null;
    }


    $order_status_row = $order_status_stmt->fetch(PDO::FETCH_ASSOC);
    $get_result['comments']  = $order_status_row['Comments'];
    $get_result['allergy']   = $order_status_row['Allergy'];
    $get_result['status']    = $order_status_row['Status'];
    $get_result['table']     = $order_status_row['FromTableID'];
    $get_result['pay_type']  = $order_status_row['PayType'];

    $query = "SELECT * FROM OrderItem WHERE OrderID = " . $order_id;
    $stmt = $conn->prepare($query);
    $query_ok = $stmt->execute();

    if (!$query_ok) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Database error";
        return null;
    }

    if ($stmt->rowCount() < 1) {
        $result['error'] = "No such order with id " . $order_id;
        return null;
    }

    while ($order_item_row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $get_result['total_price'] += $order_item_row['Price'];

        $item_details_query = "SELECT * FROM MenuItem WHERE ID = " . $order_item_row['ItemID'] . " LIMIT 1";
        $item_details_stmt = $conn->prepare($item_details_query);

        if (!$item_details_stmt->execute()) {
            header("HTTP/1.1 500 Internal Server Error");
            $result['error'] = "Error in database transaction";
            return null;
        }

        $item_details_row = $item_details_stmt->fetch(PDO::FETCH_ASSOC);
        
        $item_details_arr = array(
            "item_id"   => $order_item_row['ID'],
            "menu_id"   => $item_details_row['ID'],
            "name"      => $item_details_row['Name'],
            "price"     => $order_item_row['Price'],
            "category"  => $item_details_row['CategoryID'],
            "image_url" => $item_details_row['ImageURL'],
            "paid"      => $order_item_row['Paid']
        );

        $item_index = count($get_result['items']);
        $get_result['items'][$item_index] = $item_details_arr;
    }

    return $get_result;
}

/**
 * get_orders_by_table($table_id)
 *
 * Get all orders that belong to the given table
 */
function get_orders_by_table($table_id) {
    global $conn, $result;

    $get_result = array();

    $query = "SELECT * FROM RestaurantOrder WHERE FromTableID = '" . $table_id . "'";
	$stmt = $conn->prepare($query);
	$stmt->execute();
	
	$num = $stmt->rowCount();
	
	if ($num < 1) {
        $result['error'] = "No orders for table";
    }
    else {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $get_result['result'][0] = $row;
        $result['ok'] = true;

    }
    
    $result['result'] = $get_result;
    return $get_result;
}

function get_active_order_for_table($tableNumber) {
    global $conn, $result;

    $query  = "SELECT * FROM RestaurantOrder WHERE FromTableID = " . $tableNumber;
    $query .= " AND Status < 5 ORDER BY ID DESC LIMIT 1";
    $stmt = $conn->prepare($query);
    if (!$stmt->execute()) {
        $result['error'] = "Database error";
        return null;
    }

    if ($stmt->rowCount() < 1) {
        $result['error'] = "No active order for table " . $tableNumber;
        return null;
    }

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $order = get_order_items($row['ID']);

    $result['ok'] = true;
    $result['result'] = $order;
    return $result['result'];
}

/**
 * get_orders_with_status($status)
 *
 * Get all orders that have the given status
 */
function get_orders_with_status($status) {
    global $conn, $result;

    $get_result = array();
    $get_result['orders'] = array();
    
    $query = "SELECT ID FROM RestaurantOrder WHERE Status = '" . $status . "'";
	$stmt = $conn->prepare($query);
	$query_ok = $stmt->execute();
	
    if (!$query_ok) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "No such item in the menu";
        return;
    }

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $items = get_order_items($row['ID']);
        if (is_null($items)) continue;

        $order_i = count($get_result['orders']);
        $get_result['orders'][$order_i] = $items;
    }

    $result['ok'] = true; 
    $result['result'] = $get_result;
    return $result;
}


function add_order($table_id, $items, $comments, $allergy) {
    global $conn, $result;

    if (count($items) < 1) {
        header("HTTP/1.1 400 Bad Request");
        $result['error'] = "Must order at least one item!";
        return;
    }

    /* add order to RestaurantOrder table */



    $query  = "INSERT INTO RestaurantOrder (FromTableID, AssignedStaffID, Status, Comments, Allergy) ";
    $query .= "SELECT RestaurantTable.ID, StaffMember.ID, OrderStatusCategory.ID, '";
    $query .= $comments . "', '" . $allergy . "' ";
    $query .= "FROM RestaurantTable, StaffMember, OrderStatusCategory ";
    $query .= "WHERE RestaurantTable.ID = " . $table_id . " ";
    $query .= "AND StaffMember.ID = 1 ";
    $query .= "AND OrderStatusCategory.ID = 1";

    $add_order_stmt = $conn->prepare($query);
    $add_order_success = $add_order_stmt->execute();

    if (!$add_order_success) {
        // failed to add order
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Failed to add order: database error";
        return;
    }

    $last_order_id = $conn->lastInsertId();

    /* add each OrderItem */

    try {
        $conn->beginTransaction();

        $result['debug'] = array();
        foreach ($items as $item) {
            $add_item_query = "INSERT INTO OrderItem (OrderID, ItemID, Price) ";
            $add_item_query .= "SELECT RestaurantOrder.ID, MenuItem.ID, MenuItem.Price ";
            $add_item_query .= "FROM RestaurantOrder, MenuItem ";
            $add_item_query .= "WHERE RestaurantOrder.ID = " . $last_order_id . " ";
            $add_item_query .= "AND MenuItem.ID = " . $item;

            $conn->exec($add_item_query);
        }

        // change price of shirt if something else is also ordered with it
        $unique_items = array_unique($items);
        if (in_array("31", $unique_items) && count($unique_items) > 1) {
            $conn->exec("UPDATE OrderItem SET Price = 0.25 WHERE OrderID = " . $last_order_id . " AND ItemID = 31");
        }

        $conn->commit();
    } catch (Exception $e) {
        $conn->rollBack();
        $result['error'] = "Error in database transaction";
        $result['exception'] = $e;
        return;
    }

    $result['ok'] = true;
    $result['result']['order_id'] = $last_order_id;
}

function update_order($order_id, $new_status) {
    global $conn, $result;

    $update_query = "UPDATE RestaurantOrder SET Status = (";
    $update_query .= "OrderStatusCategory.ID FROM OrderStatusCategory WHERE ID = " . $new_status;
    $update_query .= ") WHERE RestaurantOrder.ID = " . $order_id;

    $update_stmt = $conn->prepare($update_query);
    $update_stmt->execute();

    $update_row_count = $update_stmt->rowCount();

    if ($update_row_count < 1) {
        // failed to update
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Failed to update order status";
        return;
    }

    $result['ok'] = true;
    $result['result']['new_status'] = $new_status;
}

/* determine request */
if ($_SERVER['REQUEST_METHOD'] === 'GET'){

    /* order by order id */ 
    if (isset($_GET['order_id'])) {
        $order_id = $_GET['order_id'];
        $items = get_order_items($order_id);
        if (isset($items)) {
            $result['ok'] = true;
        }
        $result['result'] = $items;
    }
    
    /* order by table id */
    else if (isset($_GET['table_id'])) {
        $table_id = $_GET['table_id'];

        if (isset($_GET['active'])) {
            if ($_GET['active'] == true)
                get_active_order_for_table($table_id);
            else
                get_orders_by_table($table_id);
        }
        else
            get_orders_by_table($table_id);
    }
    
    /* order by status */
    else if (isset($_GET['status'])) {
        $status = $_GET['status'];
        get_orders_with_status($status);
    }

    /* none of the above - error */
    else {
        header("HTTP/1.1 400 Bad Request");
        $result['error'] = "Required: order_id, table_id, or status";
    }  
    
}

/* POST */
else if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    
    if (!isset($_POST['table_id']) || !isset($_POST['items'])) {
        header("HTTP/1.1 400 Bad Request");
        $result['error'] = "Required: table_id, items";
    }

    else {
        $table_id = $_POST['table_id'];
        $items = $_POST['items'];
        $comments = isset($_POST['comments']) ? $_POST['comments'] : "";
        $allergy = isset($_POST['allergy']) ? $_POST['allergy'] : "";
        add_order($table_id, $items, $comments, $allergy);
    }
    
}

/* PUT - update status */
else if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
    
    if (!isset($_PUT['order_id']) || !isset($_PUT['status'])) {
        header("HTTP/1.1 400 Bad Request");
        $result['error'] = "Required: order_id, status";
    }

    else {
        $order_id = $_PUT['order_id'];
        $new_status = $_PUT['status'];
        update_order($order_id, $new_status);
    }
    
}

/* none of the above - method not allowed */
else {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: GET, POST, PUT"); 

    $result['error'] = "Method not allowed";
}

/* done - echo result */

echo json_encode($result);
die();

?>
