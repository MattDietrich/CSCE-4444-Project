<?php

include_once "../config/api_setup.php";
include_once "../config/database.php";
include_once "../coupon/validate.php";
include_once "../coupon/codes.php";

header("Access-Control-Allow-Methods: POST");

$db = new Database();
$conn = $db->getConnection();

$result = array();
$result['ok'] = false;
$result['error'] = null;
$result['result'] = null;

function apply_coupon($order_id, $code) {
    global $result;

    $code_id = validate_coupon($code);

    if ($code_id < 0) {
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Error validating coupon code";
        return;
    }

    if ($code_id < 1) {
        $result['error'] = "Invalid coupon code";
        return;
    }

    switch ($code_id) {
        case 1:
            apply_free_dessert($order_id);
            break;
        case 2:
            apply_meal_deal($order_id);
            break;
        case 3:
            apply_bogo_entree($order_id);
            break;
        default:
            $result['error'] = "Invalid coupon code";
            return;
    }

    $result['ok'] = true;
}

if ($_SERVER['REQUEST_METHOD'] !== "POST") {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: POST");

    $result['error'] = "Method not allowed: " . $_SERVER['REQUEST_METHOD'];
    echo json_encode($result);
    die();
}

if (!isset($_POST['order_id']) || !isset($_POST['coupon_code'])) {
    header("HTTP/1.1 400 Bad Request");

    $result['error'] = "Required: order_id, coupon_code";
    echo json_encode($result);
    die();
}

apply_coupon($_POST['order_id'], $_POST['coupon_code']);
echo json_encode($result);

?>
