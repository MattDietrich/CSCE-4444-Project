<?php

include_once "../config/api_setup.php";
include_once "../config/database.php";

function validate_coupon($raw_code) {
    $code = strtoupper($raw_code);

    $db = new Database();
    $conn = $db->getConnection();

    $query = "SELECT ID FROM CouponCodes WHERE Code = '" . $code . "'";
    $stmt = $conn->prepare($query);

    if (!$stmt->execute()) {
        error_log("Unable to execute coupon validation query");
        return -1;
    }

    if ($stmt->rowCount() < 1) {
        return 0;
    }

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return intval($row['ID']);
}

?>
