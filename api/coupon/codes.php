<?php

include_once "../config/database.php";

$db = new Database();
$conn = $db->getConnection();

function get_order_item_with_category($order_id, $category_id, $limit = 1) {
    global $conn;

    $query  = "SELECT OrderItem.ID, OrderItem.Price FROM OrderItem WHERE OrderItem.OrderID = " . $order_id;
    $query .= " AND OrderItem.Price > 0 AND EXISTS";
    $query .= " (SELECT * FROM MenuItem WHERE MenuItem.CategoryID = " . $category_id;
    $query .= " AND MenuItem.ID = OrderItem.ItemID) ORDER BY OrderItem.Price ASC";

    if ($limit > 0) {
        $query .= " LIMIT " . $limit;
    }

    $stmt = $conn->prepare($query);

    if (!$stmt->execute()) return array();
    if ($stmt->rowCount() < 1) return array();

    $result = array();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        array_push($result, $row);
    }
    return $result;
}

// free dessert
function apply_free_dessert($order_id) {
    global $conn;

    // get one dessert item from the list
    $dessert_item = get_order_item_with_category($order_id, 4);
    if (count($dessert_item) < 1) return;

    // item will be zero if not exists
    $dessert_item_id = $dessert_item[0]['ID'];

    // set dessert item price to zero
    $apply_query = "UPDATE OrderItem SET Price = 0 WHERE ID = " . $dessert_item_id;
    $apply_stmt  = $conn->prepare($apply_query);
    $apply_stmt->execute();
}

// 3.50 drink+dessert w/ purchase of entree
function apply_meal_deal($order_id) {
    global $conn;

    // entry condition: has entree
    $entree_item = get_order_item_with_category($order_id, 1);
    if (count($entree_item) < 1) return;

    // check that we have everything required

    $dessert_item = get_order_item_with_category($order_id, 4);
    if (count($dessert_item) < 1) return;

    $drink_item = get_order_item_with_category($order_id, 6);
    if (count($drink_item) < 1) return;

    // set drink price to 3.50 minus dessert price

    $drink_price = 3.50 - floatval($dessert_item[0]['Price']);
    $drink_item_id = $drink_item[0]['ID'];
    $drink_price_stmt = $conn->prepare("UPDATE OrderItem SET Price = " . $drink_price . " WHERE ID = " . $drink_item_id);
    $drink_price_stmt->execute();
}

// buy one entree, get one free
function apply_bogo_entree($order_id) {
    global $conn;

    // entry condition: has two entrees
    $entree_items = get_order_item_with_category($order_id, 1, 2);
    if (count($entree_items) < 2) return;

    $free_entree_id = $entree_items[0]['ID'];
    $bogo_stmt = $conn->prepare("UPDATE OrderItem SET Price = 0 WHERE ID = " . $free_entree_id);
    $bogo_stmt->execute();
}

?>

