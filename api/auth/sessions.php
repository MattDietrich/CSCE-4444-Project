<?php

function get_session_by_user_id($user_id) {
    $db = new Database();
    $conn = $db->getConnection();

    $query = "SELECT * FROM UserSession WHERE UserID = " . $user_id . " ORDER BY ID DESC";
    $stmt = $conn->prepare($query);
    $stmt->execute();

    $num_rows = $stmt->rowCount();

    if ($num_rows == 0) {
        return null;
    }

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row;
}

function get_session_by_token($token) {
    $db = new Database();
    $conn = $db->getConnection();

    $query = "SELECT * FROM UserSession WHERE ID = " . $token . " ORDER BY ID DESC";
    $stmt = $conn->prepare($query);
    $stmt->execute();

    $num_rows = $stmt->rowCount();
    if ($num_rows == 0) {
        return null;
    }

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row;
}

function clear_sessions_by_user($user_id) {
    $db = new Database();
    $conn = $db->getConnection();

    $query = "DELETE FROM UserSession WHERE UserID = " . $user_id;
    $stmt = $conn->prepare($query);
    $stmt->execute();
}

function delete_session($token) {

}

function start_session($user_id) {
    $db = new Database();
    $conn = $db->getConnection();

    // create a new, unique session ID

    // mother of god, this query.
    $create_query = "SELECT new_id FROM (SELECT FLOOR(RAND() * 99999) AS new_id UNION SELECT FLOOR(RAND() * 99999) AS new_id) AS ids WHERE `new_id` NOT IN (SELECT ID FROM UserSession) LIMIT 1";
    $create_stmt = $conn->prepare($create_query);
    $create_stmt->execute();
    $create_num_rows = $create_stmt->rowCount();

    // shouldn't ever have zero rows, but if we do, just return null
    if ($create_num_rows == 0) {
        return null;
    }

    // we have an id. insert it into the table.

    $create_row = $create_stmt->fetch(PDO::FETCH_ASSOC);
    $new_id = $create_row['new_id'];

    $put_query = "INSERT INTO UserSession (ID, UserID) SELECT " . $new_id . ", ID FROM SystemUser WHERE ID = " . $user_id;
    $put_stmt = $conn->prepare($put_query);
    $put_stmt->execute();

    return $new_id;
}

function validate_token($token, $restrictToManager = false) {
    /* $restrictToManager -- Only return true if the token is associated with an active
        session AND the user associated with that session is a manager */

    $db = new Database();
    $conn = $db->getConnection();

    $query = "SELECT * FROM UserSession WHERE ID = " . $token;
    if ($restrictToManager) $query .= " AND UserID IN (SELECT UserID FROM StaffMember WHERE CategoryID = 3)";

    $stmt = $conn->prepare($query);
    $stmt->execute();

    return $stmt->rowCount() != 0;
}
?>
