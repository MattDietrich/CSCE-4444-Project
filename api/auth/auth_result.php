<?php

abstract class AuthResult {
    const SUCCESS = 0;
    const NO_USER = 1;
    const INVALID_PASS = 2;
    const DUPE_USER = 3;
}

?>

