<?php

include_once "../config/database.php";
include_once "auth_result.php";

function attempt_auth($username, $password) {
    $result = array();

    // establish database connection
    $db = new Database();
    $conn = $db->getConnection();

    // TODO handle database connection error

    // prepare and execute statement
    $query = "SELECT ID, Password FROM SystemUser WHERE Username = '" . $username . "'";
    $stmt = $conn->prepare($query);
    $stmt->execute();

    // check that there is only one row
    
    $num = $stmt->rowCount();

    if ($num < 1) {
        // no user
        $result['code'] = AuthResult.NO_USER;
        return $result;
    }

    if ($num > 1) {
        // too many users? big error
        $result['code'] = AuthResult.DUPE_USER;
        return $result;
    }

    // check given password against stored password

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $storedPassword = $row['Password'];

    if ($storedPassword !== $password) {
        // invalid password
        $result['code'] = AuthResult.INVALID_PASS;
        return $result;
    }

    // success
    $result['code'] = AuthResult.SUCCESS;
    $result['user_id'] = $row['ID'];
    return $result;
}

?>
