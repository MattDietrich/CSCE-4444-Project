<?php

require_once "config/api_setup.php";

/*
 *  NOT YET IMPLEMENTED - as of 21:53 3 Apr 2018 - Shawn
 *  Will always return 501 Not Yet Implemented because... yeah.
 */

$result = array();
$result["ok"] = false;
$result["message"] = "Not yet implemented";

$result["username"] = $_POST["username"];

header('HTTP/1.1 501 Not Yet Implemented');
die(json_encode($result));

?>
