<?php

include_once '../config/database.php';

abstract class TodoObject {

    protected $conn;
    protected $table_name;

    public function __construct($table_name) {
        $this->table_name = $table_name;

        $database = new Database();
        $this->conn = $database->getConnection();
    }

    private function do_query($query) {
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public function read_all() {
        $query = "SELECT * FROM " . $this->table_name . ";";
        return $this->do_query($query);
    }

    public function read_by_id($id) {
        $query = "SELECT * FROM " . $this->table_name . " WHERE ID = " . $id . ";";
        return $this->do_query($query);
    }
}

?>
