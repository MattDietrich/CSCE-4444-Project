<?php

include_once 'object.class.php';

class Table extends TodoObject {

    public $id;
    public $num_customers;
    public $needs_refill;
    public $needs_help;

    public function __construct() {
        parent::__construct("RestaurantTable");
    }

}

?>
