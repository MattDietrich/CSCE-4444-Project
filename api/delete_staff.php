<?php
    include_once "config/api_setup.php";
    include_once "config/database.php";
    include_once "auth/sessions.php";

    header("Access-Control-Allow-Methods: POST");

    $result = array();
    $result['ok'] = false;
    $result['error'] = null;
    $result['result'] = null;

    $database = new Database();
    $conn = $database->getConnection();

    function delete_staff($UserID) {
        global $conn, $result;

        $stmt = $conn->prepare("DELETE FROM StaffMember WHERE ID = :id");
        $stmt->bindValue(':id', $UserID, PDO::PARAM_INT);

        if (!$stmt->execute()) {
            header("HTTP/1.1 500 Internal Server Error");
            $error = $stmt->errorInfo();
            $result['error'] = "Database Error (" . $error[1] + "): " . $error[2];
            return;
        }

        $result['ok'] = true;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        if(!isset($_POST['UserID']) || !isset($_POST['token'])) {
            header("HTTP/1.1 400 Bad Request");
            $result['error'] = "Required: UserID, token";
        }

        else if (!validate_token($_POST['token'], true)) {
            header("HTTP/1.1 403 Forbidden");
            $result['error'] = "Token must be associated with active manager session.";
        }

        else {
            $UserID = $_POST['UserID'];
            delete_staff($UserID);
        }
    }

    else {
        header("HTTP/1.1 405 Method Not Allowed");
        header("Allow: POST"); 

        $result['error'] = "Method not allowed";
    }

    echo json_encode($result);
    
?>
