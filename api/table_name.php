<?php
include_once "config/api_setup.php";
include_once "config/database.php";
include_once "auth/sessions.php";

define("INVALID_TABLE", -1);

header("Access-Control-Allow-Methods: GET, POST");

$result = array();
$result['ok'] = false;
$result['result'] = null;
$result['error'] = null;

$db = new Database();
$conn = $db->getConnection();

function get_table_name($tableID) {
    global $conn;

    $query = "SELECT Name FROM RestaurantTable WHERE ID = " . $tableID;
    $stmt = $conn->prepare($query);
    $stmt->execute();

    // If rows were returned, extract the name
    if ($stmt->rowCount() > 0) {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        extract($row);
        return $Name;
    }

    // If no rows returned, return error
    else return INVALID_TABLE;
}

function set_table_name($tableID, $newName) {
    global $conn;

    // First ensure that the table ID is valid
    $query = "SELECT ID FROM RestaurantTable WHERE ID = " . $tableID . ";";
    $stmt = $conn->prepare($query);
    $stmt->execute();


    if ($stmt->rowCount() > 0) {
        if (is_null($newName)) {
            $stmt = $conn->prepare("UPDATE RestaurantTable SET Name = NULL WHERE ID = :id");
            $stmt->bindValue(':id', $tableID, PDO::PARAM_INT);
            $stmt->execute();
            return 0;
        }

        $query = "UPDATE RestaurantTable SET Name = '" . $newName . "' WHERE ID = " . $tableID . ";";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        return 0;
    }

    else return INVALID_TABLE;
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (!isset($_GET['token']) || !isset($_GET['tableID'])) {
        header("HTTP/1.1 400 Bad request");
        $result['error'] = "Required: token, tableID";
    }

    else if (!validate_token($_GET['token'])) {
        header("HTTP/1.1 403 Forbidden");
        $result['error'] = "Invalid token";
    }

    else {
        $tableName = get_table_name($_GET['tableID']);
        if ($tableName === INVALID_TABLE) {
            $result['error'] = "Table does not exist";
        }

        else {
            $result['okay'] = true;
            $result['result'] = $tableName;
        }
    }
}

else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!isset($_POST['token'])  || !isset($_POST['tableID'])) {
        header("HTTP/1.1 400 Bad request");
        $result['error'] = "Required: token, tableID";
    }

    else if (!validate_token($_POST['token'])) {
        header("HTTP/1.1 403 Forbidden");
        $result['error'] = "Invalid token";
    }

    else {
        $table_name = isset($_POST['name']) ? $_POST['name'] : null;
        if (set_table_name($_POST['tableID'], $table_name) === INVALID_TABLE) {
            $result['error'] = "Table #" . $tableID . " does not exist";
        }

        else {
            $result['okay'] = true;
            $result['result'] = "Table name updated";
        }
    }
}

else {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: GET, POST");
    $result['error'] = "Method not allowed";
}

echo json_encode($result);
?>
