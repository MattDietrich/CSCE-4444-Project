<?php

include_once "config/api_setup.php";
include_once "config/database.php";
include_once "auth/sessions.php";

header("Access-Control-Allow-Methods: POST");

// result default values
$result = array();
$result['ok'] = false;
$result['result'] = null;
$result['error'] = null;

// database connection
$db = new Database();
$conn = $db->getConnection();


function update_order($order_id, $new_status, $token) {
    global $conn, $result;

    if (!validate_token($token)) {
        header("HTTP/1.1 403 Forbidden");
        $result['error'] = "Invalid token";
    }

    else{

        $update_stmt = $conn->prepare("UPDATE RestaurantOrder SET Status = :status WHERE ID = :id");
        $update_stmt->bindValue(':status', $new_status, PDO::PARAM_INT);
        $update_stmt->bindValue(':id', $order_id, PDO::PARAM_INT);

        if (!$update_stmt->execute()) {
            header("HTTP/1.1 500 Internal Server Error");
            $error = $update_stmt->errorInfo();
            $result['error'] = "Database Error ($error[1]): $error[2]";
            return;
        }

        $result['ok'] = true;
        $result['result']['new_status'] = $new_status;
    }
}


/* POST */
if ($_SERVER['REQUEST_METHOD'] === 'POST'){
	
    if (!isset($_POST['order_id']) || !isset($_POST['status'])) {
        header("HTTP/1.1 400 Bad Request");
        $result['error'] = "Required: order_id, status, token";
    }

    else {
        $order_id = $_POST['order_id'];
        $status = $_POST['status'];
        $token = $_POST['token'];
        update_order($order_id, $status, $token);
    }
    
}

/* none of the above - method not allowed */
else {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: POST"); 

    $result['error'] = "Method not allowed";
}

/* done - echo result */

echo json_encode($result);

?>
