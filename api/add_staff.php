<?php
    include_once "config/api_setup.php";
    include_once "config/database.php";
    include_once "auth/sessions.php";

    header("Access-Control-Allow-Methods: POST");

    $result = array();
    $result['ok'] = false;
    $result['error'] = null;
    $result['result'] = null;

    $database = new Database();
    $conn = $database->getConnection();

    function add_staff($user,$pass,$first, $last, $category)
    {
        global $conn, $result;


        // get the biggest user ID and add one

        $user_id_query = "SELECT ID FROM SystemUser ORDER BY ID DESC LIMIT 1";
        $user_id_stmt = $conn->prepare($user_id_query);

        if(!$user_id_stmt->execute() || $user_id_stmt->rowCount() < 1)
        {
            header("HTTP/1.1 500 Internal Server Error");
            $error = $user_id_stmt->errorInfo();
            $result['error'] = "Database Error (" . $error[1] . "): " . $error[2];
            return;
        }

        $user_id = intval($user_id_stmt->fetch(PDO::FETCH_ASSOC)['ID']) + 1;


        // get the biggest staff member ID and add one

        $member_id_query = "SELECT ID FROM StaffMember ORDER BY ID DESC LIMIT 1"; 
        $member_id_stmt = $conn->prepare($member_id_query);

        if(!$member_id_stmt->execute() || $member_id_stmt->rowCount() < 1)
        {
            header("HTTP/1.1 500 Internal Server Error");
            $error = $member_id_stmt->errorInfo();
            $result['error'] = "Database Error (" . $error[1] . "): " . $error[2];
            return;
        }

        $member_id = intval($member_id_stmt->fetch(PDO::FETCH_ASSOC)['ID']) + 1;


        // insert system user first

        $insert_user_query  = "INSERT INTO SystemUser (ID, Username, Password) VALUES (";
        $insert_user_query .= $user_id . ", '" . $user . "', '" . $pass . "')";
        $insert_user_stmt   = $conn->prepare($insert_user_query);

        if(!$insert_user_stmt->execute())
        {
            header("HTTP/1.1 500 Internal Server Error");
            $error = $insert_user_stmt->errorInfo();
            $result['error'] = "Database Error (" . $error[1] . "): " . $error[2];
            return;
        }


        // insert staff member next

        $insert_staff_query  = "INSERT INTO StaffMember (ID, FirstName, LastName, CategoryID, UserID) ";
        $insert_staff_query .= "SELECT " . $member_id . ", '" . $first . "', '" . $last . "', " . $category . ", ";
        $insert_staff_query .= "SystemUser.ID FROM SystemUser WHERE SystemUser.ID = " . $user_id;

        $insert_staff_stmt = $conn->prepare($insert_staff_query);
        if(!$insert_staff_stmt->execute())
        {
             header("HTTP/1.1 500 Internal Server Error");
             $error = $insert_staff_stmt->errorInfo();
             $result['error'] = "Database Error (" . $error[1] . "): " . $error[2];
             return;
        }

        $result['ok'] = true;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        if(!isset($_POST['first']) || !isset($_POST['last']) || !isset ($_POST['category']) || !isset($_POST['token']))
        {
            header("HTTP/1.1 400 Bad Request");
            $result['error'] = "Required: FirstName, LastName, category, token";
        }

        else if(!validate_token($_POST['token'], true)) {
            header("HTTP/1.1 403 Forbidden");
            $result['error'] = "Token must be associated with an active manager session.";
        }

        else
        {
            $first = $_POST['first'];
            $last = $_POST['last'];
            $category = $_POST['category'];
            $user = $_POST['user'];
            $pass = $_POST['pass'];

            add_staff($user,$pass,$first, $last, $category);
        }
    }
    else {
        header("HTTP/1.1 405 Method Not Allowed");
        header("Allow: POST"); 

        $result['error'] = "Method not allowed";
    }

    echo json_encode($result);
?>
