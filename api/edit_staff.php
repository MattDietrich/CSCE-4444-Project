<?php
    include_once "config/api_setup.php";
    include_once "config/database.php";
    include_once "auth/sessions.php";

    header("Access-Control-Allow-Methods: POST");

    $result = array();
    $result['ok'] = false;
    $result['error'] = null;
    $result['result'] = null;

    $database = new Database();
    $conn = $database->getConnection();

    function edit_staff($id, $first, $last ,$category)
    {
        global $conn, $result;

        $query  = "UPDATE StaffMember";
        $query .= " SET FirstName = :first, LastName = :last, CategoryID = :category";
        $query .= " WHERE ID = :id";

        $stmt = $conn->prepare($query);
        $stmt->bindValue(':first',    $first,    PDO::PARAM_STR);
        $stmt->bindValue(':last',     $last,     PDO::PARAM_STR);
        $stmt->bindValue(':category', $category, PDO::PARAM_INT);
        $stmt->bindValue(':id',       $id,       PDO::PARAM_INT);

        if(!$stmt->execute())
        {
            header("HTTP/1.1 500 Internal Server Error");
            $error = $stmt->errorInfo();
            $result['error'] = "Database Error (" + $error[1] + "): " + $error[2];
            return;
        }

        $result['ok'] = true;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        if (!isset($_POST['token']) || !validate_token($_POST['token'], true)) {
            header("HTTP/1.1 403 Forbidden");
            $result['error'] = "Token must be associated with active manager session";
        }

        else {
            $id = $_POST['id'];
            $first = $_POST['first'];
            $last = $_POST['last'];
            $category = $_POST['category'];

            edit_staff($id, $first,$last,$category);
        }
    }

    else {
        header("HTTP/1.1 405 Method Not Allowed");
        header("Allow: POST"); 
        $result['error'] = "Method not allowed";
    }

    echo json_encode($result);
?>
