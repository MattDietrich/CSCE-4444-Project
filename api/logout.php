<?php
include_once "config/api_setup.php";
include_once "config/database.php";
include_once "auth/auth_result.php";
include_once "auth/auth_functions.php";
include_once "auth/sessions.php";

header("Access-Control-Allow-Methods: POST");

$result = array();
$result['ok'] = false;
$result['error'] = null;
$result['result'] = null;

if (!$_POST) {
  header("HTTP/1.1 405 Method Not Allowed");
  header("Allow: POST");
}

// If no token provided
if (!isset($_POST['token'])) {
  header("HTTP/1.1 400 Bad Request");
  $result['error'] = "Token must be provided";
  echo(json_encode($result));
  die();
}

$token = $_POST['token'];

$database = new Database();
$conn = $database->getConnection();

$query = "SELECT * FROM UserSession WHERE ID = " . $token;
$stmt = $conn->prepare($query);
$stmt->execute();

// If the token was not found
if ($stmt->rowCount() == 0) {
  header ("HTTP/1.1 403 Forbidden");
  $result['error'] = "Session token not found";
  echo (json_encode($result));
  die();
}

$query = "DELETE FROM UserSession WHERE ID = " . $token;
$stmt = $conn->prepare($query);
$stmt->execute();

header("HTTP/1.1 200 OK");
$result['ok'] = true;
echo (json_encode($result));
?>
