<?php

include_once "config/api_setup.php";
include_once "config/database.php";

include_once "auth/auth_result.php";
include_once "auth/auth_functions.php";
include_once "auth/sessions.php";


// allow only POST
header("Access-Control-Allow-Methods: POST");

// result and default values
$result = array();
$result['ok'] = false;
$result['error'] = null;
$result['result'] = null;


// only POST is supported
if (!$_POST) {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: POST");

    $result['error'] = "Method not allowed";
    echo(json_encode($result));
    die();
}


// username and password must both be provided
if (!isset($_POST['username']) || !isset($_POST['password'])) {
    header("HTTP/1.1 400 Bad Request");

    $result['error'] = "Username and password are required";
    echo(json_encode($result));
    die();
}


// get username and password from POST request (we have verified that they have been given)
$username = $_POST['username'];
$password = $_POST['password'];


// attempt to auth and use the result of the attempt

$auth = attempt_auth($username, $password);

switch ($auth['code']) {
    case AuthResult.NO_USER:
        header("HTTP/1.1 401 Unauthorized");
        $result['error'] = "User not found";
        break;
    case AuthResult.INVALID_PASS:
        header("HTTP/1.1 401 Unauthorized");
        $result['error'] = "Invalid password";
        break;
    case AuthResult.SUCCESS:
        header("HTTP/1.1 200 OK");
        $result['ok'] = true;
        break;
    default:
        // AuthUser.DUPE_USER or anything else weird
        header("HTTP/1.1 500 Internal Server Error");
        $result['error'] = "Internal server error";
        break;
}

$result['result'] = array();

if ($auth['code'] != AuthResult.SUCCESS) {
    echo(json_encode($result));
    die();
}

/* auth successful. */

if (isset($_POST['create_session'])) {
    $create_session = filter_var($_POST['create_session'], FILTER_VALIDATE_BOOLEAN);
    if ($create_session) {
        $session_id = start_session($auth['user_id']);
        $result['result']['token'] = $session_id;
    }
}

$result['result']['username'] = $username;

echo(json_encode($result));
die();

?>
