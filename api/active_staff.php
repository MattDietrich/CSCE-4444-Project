<?php
/* Get a list of staff members with active sessions */

include_once "config/api_setup.php";
include_once "config/database.php";
include_once "auth/sessions.php";

header("Access-Control-Allow-Methods: GET");

$result = array();
$result['ok'] = false;
$result['error'] = null;
$result['result'] = null;

if ($_SERVER['REQUEST_METHOD'] !== "GET") {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: GET");

    $result['error'] = "Method not allowed";
    echo(json_encode($result));
    die();
}

else if (!isset($_GET['token'])) {
    header("HTTP/1.1 400 Bad Request");
    $result['error'] = "Missing token";
    echo(json_encode($result));
    die();
}

else if (!validate_token($_GET['token'], true)) {
    header("HTTP/1.1 403 Forbidden");
    $result['error'] = "Invalid token. Token must be associated with active manager session.";
    echo(json_encode($result));
    die();
}

$database = new Database();
$conn = $database->getConnection();

$query = "SELECT * FROM StaffMember WHERE UserID IN (SELECT UserID FROM UserSession)";
$stmt = $conn->prepare($query);
$stmt->execute();

$activeStaffList = array();

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($row);
    $currentMember = array (
        "UserID" => $UserID,
        "firstName" => $FirstName,
        "lastName" => $LastName
    );

    array_push($activeStaffList, $currentMember);
}

$result['ok'] = true;
$result['result'] = $activeStaffList;

echo json_encode($result);
?>
