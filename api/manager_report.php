<?php
/*
 API that will output manager log
 WILL NEED MANAGER VERIFICATION IMPLEMENTED

 report will include
 - # of customers
 - # of staff
 - # of orders
 - most popular item
 - revenue 
 - comped values
*/

include_once "config/api_setup.php";
include_once "config/database.php";
include_once "auth/sessions.php";


header("Access-Control-Allow-Methods: GET");

$db = new Database();
$conn = $db->getConnection();

$result = array();
$result['ok'] = false;
$result['error'] = null;
$result['result'] = null;

function make_report() {
    global $conn, $result;

    /* num of customers */
    $cquery = "SELECT COUNT(ID) FROM Customer";
    $cstmt = $conn->prepare($cquery);
    $cstmt->execute();
    $crow = $cstmt->fetch(PDO::FETCH_ASSOC);

    /* num of staff */
    $squery = "SELECT COUNT(ID) FROM StaffMember";
    $sstmt = $conn->prepare($squery);
    $sstmt->execute();
    $srow = $sstmt->fetch(PDO::FETCH_ASSOC);

    /* num of total orders */ 
    $oquery = "SELECT COUNT(ID) FROM RestaurantOrder";
    $ostmt = $conn->prepare($oquery);
    $ostmt->execute();
    $orow = $ostmt->fetch(PDO::FETCH_ASSOC);

    /* find most popular item ID*/
    $iquery = "SELECT ItemID, COUNT(ItemID) AS occurrence FROM OrderItem GROUP BY ItemID ORDER BY occurrence DESC LIMIT 1";
    $istmt = $conn->prepare($iquery);
    $istmt->execute();
    $irow = $istmt->fetch(PDO::FETCH_ASSOC);
    $menu_item = $irow['ItemID'];

    /* use ID to find item name */
    $mquery = "SELECT Name, Price, ID FROM MenuItem WHERE ID = " . $menu_item;
    $mstmt = $conn->prepare($mquery);
    $mstmt->execute();
    $mrow = $mstmt->fetch(PDO::FETCH_ASSOC);
    $mrow['Price'] = round($mrow['Price']);
    $mrow['ID'] = intval($mrow['ID']);

    /* total revenue */
    $rquery = "SELECT SUM(Price) FROM OrderItem";
    $rstmt = $conn->prepare($rquery);
    $rstmt->execute();
    $rrow = $rstmt->fetch(PDO::FETCH_ASSOC);

    /* comped orders */
    $comped_count_stmt = $conn->prepare("SELECT COUNT(ID) FROM CompedOrders");
    $comped_count_stmt->execute();
    $comped_count = $comped_count_stmt->fetch(PDO::FETCH_ASSOC)['COUNT(ID)'];

    $comped_by_stmt = $conn->prepare("SELECT StaffID, COUNT(StaffID) AS Occurrences FROM CompedOrders GROUP BY StaffID ORDER BY Occurrences DESC");
    $comped_by_stmt->execute();
    $comped_by = array();
    while ($tmp_row = $comped_by_stmt->fetch(PDO::FETCH_ASSOC)) {
        $this_user_stmt = $conn->prepare("SELECT FirstName, LastName FROM StaffMember WHERE ID = :id");
        $this_user_stmt->bindValue(':id', $tmp_row['StaffID'], PDO::PARAM_INT);
        $this_user_stmt->execute();
        $this_user = $this_user_stmt->fetch(PDO::FETCH_ASSOC);

        $tmp_row['Name'] = $this_user['FirstName'] . ' ' . $this_user['LastName'];
        array_push($comped_by, $tmp_row);
    }

    /* loss on comped orders */
    $comped_loss_stmt = $conn->prepare("SELECT SUM(OrderItem.Price) AS Loss FROM CompedOrders, OrderItem WHERE OrderItem.OrderID = CompedOrders.OrderID");
    $comped_loss_stmt->execute();
    $comped_loss = $comped_loss_stmt->fetch(PDO::FETCH_ASSOC)['Loss'];

    $report_arr = array(
        "num_of_customers" => intval($crow['COUNT(ID)']),
        "num_of_staff" => intval($srow['COUNT(ID)']),
        "num_of_orders" => intval($orow['COUNT(ID)']),
        "most_popular_item" => $mrow,
        "revenue" => round($rrow['SUM(Price)'], 2),
        "num_comped_orders" => intval($comped_count),
        "comped_orders" => $comped_by,
        "comp_loss" => round($comped_loss, 2)
    );

    return $report_arr;
}

/* validate if manager */
if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    header("HTTP/1.1 405 Method Not Allowed");
    header("Allow: GET");

    $result['error'] = "Method not allowed";
    echo(json_encode($result));
    die();
}

else if (!isset($_GET['token'])) {
    header("HTTP/1.1 400 Bad Request");
    $result['error'] = "Missing token";
    echo(json_encode($result));
    die();
}

else if (!validate_token($_GET['token'], true)) {
    header("HTTP/1.1 403 Forbidden");
    $result['error'] = "Invalid token. Token must be associated with active manager session.";
    echo(json_encode($result));
    die();
}

$result['ok'] = true;
$result['result'] = make_report();
echo json_encode($result);

?>
