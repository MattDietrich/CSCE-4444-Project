<?php

if (!isset($_COOKIE['todo_internal_token'])) {
    header("Location: ..");
}

// not worried about having a table number here
// don't want something to go wrong and deny customer access to drawing

?>

<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
    <link rel="stylesheet" href="/mockups/static/table.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="#">#TODO</a>
      </div>
    </nav>
    <div class="container-fluid"></div>
    <div class="container">
      <h2 class="mb-5 pb-2">Would you like to enter a drawing for a chance to win a FREE dessert?</h1>
      <div class="row" id="buttons-wrapper">
        <div class="col-3 offset-3">
            <button class="btn btn-primary btn-lg btn-block" id="yes">Yes</button>
		</div>
		<div class="col-3">
		  <a href=".">
			<button class="btn btn-primary btn-lg btn-block" id="no">No</button>
		  </a>
        </div>
      </div>
        <div id="drawingWin" style="display:none">
            <h3>You win!</h3>
            <p>Use coupon code <span class="font-weight-bold">SAVEROOM</span> for a free dessert on your next order.</p>
            <a href="." class="btn btn-lg btn-primary">Done</a>
        </div>
        <div id="drawingLoss" style="display:none">
            <h3>Oh no!</h3>
            <p>Better luck next time!</p>
            <a href="." class="btn btn-lg btn-primary">Done</a>
        </div>
    </div>
    <div class="container-fluid">
      <div class="mt-5" id="spacer"></div>
      <div class="fixed-bottom mb-3 mr-3">
        <div class="row justify-content-end">
          <div class="col-2 offset-10">
            <button class="btn btn-info btn-block" id="btnRefill">Refill</button>
            <button class="btn btn-danger btn-block" id="btnHelp">Help</button>
          </div>
        </div>
      </div>
    </div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="../scripts/drawAdminMenu.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script type="text/javascript">
function onYes() {
    $('#buttons-wrapper').hide();
    var result = Math.floor(Math.random() * Math.floor(5));
    if (result == 1)
        $('#drawingWin').show();
    else
        $('#drawingLoss').show();
};

        $(() => {
            drawAdminMenu();
            $('#yes').click(onYes);
        });

    </script>
  </body>
</html>
