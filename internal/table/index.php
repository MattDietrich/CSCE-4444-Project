<?php

if (!isset($_COOKIE['todo_internal_token'])) {
    header("Location: ..");
}

if (!isset($_COOKIE['todo_table_number'])) {
    header("Location: table_number");
}

?>

<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
    <link rel="stylesheet" href="/mockups/static/table.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="#">#TODO</a>
      </div>
    </nav>
    <div class="container-fluid"></div>
    <div class="container">
      <h4 class="text-center mb-5">Table <span id="title-number"></span><span id="title-name" class="invisible"></span></h4>
      <div class="row" id="buttons-wrapper">
        <div class="col-6 offset-3">
          <div class="row mb-4" id="buttons-top">
            <div class="col-6">
			  <a href="menu">
                <button class="btn btn-primary btn-lg btn-block" id="btnMenu">Menu</button>
			  </a>
            </div>
            <div class="col-6">
			  <a href="games">
                <button class="btn btn-primary btn-lg btn-block" id="btnGames">Games</button>
			  </a>
            </div>
          </div>
          <div class="row" id="buttons-bottom">
            <div class="col-12">
              <button class="btn btn-primary btn-lg btn-block" id="btnOrder">Order</button>
            </div>
          </div>
        </div>
      </div>
    </div>
	<div class="container">
		<form id="name-form">
			<div class="form-group" id="table-name">
				<label for="table-name"><b>Enter Table Name (optional):</b></label>
				<input type="text" class="form-control" placeholder="Name" name="table-name" required>
			</div>
			<div class="my-1 py-1"></div>
			<div id="table-name-submit">
				<button class="btn btn-primary" type="submit">Confirm</button>
			</div>
        </form>
	</div>
    <div class="my-4 py-4 invisible"></div>
    <div class="container-fluid">
      <div class="mt-5" id="spacer"></div>
        <div class="row justify-content-end fixed-bottom mb-3 mr-3">
          <div class="col-2 offset-10">
            <button class="btn btn-info btn-block" id="btnRefill">Refill</button>
            <button class="btn btn-danger btn-block" id="btnHelp">Help</button>
          </div>
      </div>
    </div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="../scripts/drawAdminMenu.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script type="text/javascript">
var statusMessages = {
    '1': 'Order received by kitchen',
    '2': 'Order cooking...',
    '3': 'Order on the way',
    '4': 'Payment'
};

function showTitle(title) {
    $('#title-name').removeClass("invisible");
    $('#title-name').empty().append(' - ' + title);
    $('#name-form').addClass("invisible");
};

function hideTitle() {
    $('#title-name').addClass("invisible");
    $('#title-name').empty();
    $('#name-form').removeClass("invisible");
};

function drawTitle() {
	var tableNumber = Cookies.get('todo_table_number');
	var tableToken = Cookies.get('todo_internal_token');
    $('#title-number').text(Cookies.get('todo_table_number') || '#');
    $.get("../../api/table_name", { token: tableToken, tableID: tableNumber }, res => {
		var cb_result = res['result'];
		
		if(res['okay'] && res['result'])
            showTitle(res['result']);
        else
            hideTitle();
		
	}).fail(console.log);
};

function fetchAndRefresh() {
    fetchOrder(refreshOrderStatus);
	drawTitle();
};

function fetchOrder(callback) {
    var tableNumber = Cookies.get('todo_table_number');
    $.get("../../api/order", { table_id: tableNumber, active: true }, res => {
        var cb_result = res['result'];

        if (!res['ok'] || !res['result']) {
            // no active order, or failed to get order
            Cookies.remove('todo_order_id');
            Cookies.remove('todo_payment_ready');
            return callback(null);
        }
            
        // have an active order
        Cookies.set('todo_order_id', res['result']['order_id']);

        // payment ready?
        Cookies.set('todo_payment_ready', res['result']['status'] == '4' ? "true" : "false");

        return callback(cb_result);

    }).fail(console.log);
};

function refreshOrderStatus(order) {
    if (!order) {
        $('#btnOrder').text("Order");
        return;
    };

    var orderStatus = order['status'];
    var statusMsg = statusMessages[orderStatus];

    $('#btnOrder').text(statusMsg);
}

$(() => {
	drawAdminMenu();

	fetchAndRefresh();
    setInterval(fetchAndRefresh, 5000);

	Cookies.remove('todo_payment_order_id');

	var table_id = Cookies.get('todo_table_number');

	$('#btnOrder').click(() => {
		if (!Cookies.get('todo_order_id')) {
			window.location = "ordering";
			return;
		}

		if (Cookies.get('todo_payment_ready') === "true") {
			window.location = "payment";
		}

		// TODO show modal with order info
	});

	$('#btnRefill').click(() => {
		$.post("../../api/table/update.php", { table_id: table_id, needs_refill: true }, res => {
			alert("Refill request received. We're on the way!");
		});
	});

	$('#btnHelp').click(() => {
		$.post("../../api/table/update.php", { table_id: table_id, needs_help: true }, res => {
			alert("Help request received. We're on the way!");
		});
	});
	
	$('#name-form').submit(() => {
        var name_input = $('#table-name input').val();
		var tableNumber = Cookies.get('todo_table_number');
		var tableToken = Cookies.get('todo_internal_token');

		$.post("../../api/table_name.php", { token : tableToken, tableID : tableNumber, name : name_input }, res => {
            window.location.reload();
        });

        return false;
    });
});

    </script>
  </body>
</html>
