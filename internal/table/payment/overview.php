<?php

if (!isset($_COOKIE['todo_internal_token'])) {
    header("Location: ..");
}

if (!isset($_COOKIE['todo_table_number'])) {
    header("Location: table_number");
}

?>

<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
    <link rel="stylesheet" href="/mockups/static/table.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="#">#TODO</a>
      </div>
    </nav>
    <div class="container-fluid"></div>
    <div class="container">
        <div class="row">
            <div class="col-8">
                <h3 class="my-3 pb-3 border-bottom">Unpaid Items</h3>
                <ul class="my-3 pt-3" id="unpaid-items"></ul>
                <h4 class="my-3 py-3">Total: <span class="font-weight-bold" id="total-price"></span></h4>
            </div>
            <div class="mt-3 col-3 offset-1">
                <a href="pay_full" class="btn btn-lg btn-block btn-success">Pay</a>
                <a class="btn btn-lg btn-block btn-info" href="pay_split">Split Payment</a>
            </div>
        </div>
    </div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="../../scripts/drawAdminMenu.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
<script type="text/javascript">
function fetchUnpaidItems(callback) {
    var table = Cookies.get('todo_table_number');
    $.get("../../../api/order", { table_id: table, active: true }, res => {
        if (!res['ok'] || res['result']['items'].length < 1) {
            return callback([]);
        }

        if (res['result']['pay_type'] != "1") {
            window.location = "complete";
        }

        $('#total-price').text(res['result']['total_price']);
        var unpaid = res['result']['items'].filter(i => i['paid'] == "0");
        return callback(unpaid);
    });
};

function drawUnpaidItems(items) {
    if (items.length < 1) {
        $.post('../../../api/order_update', {
            token: Cookies.get('todo_internal_token'),
            order_id: Cookies.get('todo_order_id'),
            status: 6
        }, res => {
            window.location = "complete";
        }).fail(res => {
            console.error(res);
        });
    }

    for (var i in items) {
        var item = items[i];
        var item_li = $('<li></li>').text(item['name'] + ' - ' + item['price']);
        $('#unpaid-items').append(item_li);
    }
};

        $(() => {
            Cookies.remove('todo_coupon_used');
            drawAdminMenu();
            fetchUnpaidItems(drawUnpaidItems);
        });
    </script>
  </body>
</html>
