<?php

if (!isset($_COOKIE['todo_internal_token'])) {
    header("Location: ..");
}

if (!isset($_COOKIE['todo_table_number'])) {
    header("Location: table_number");
}

?>

<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
    <link rel="stylesheet" href="/mockups/static/table.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="#">#TODO</a>
      </div>
    </nav>
    <div class="container-fluid"></div>
    <div class="container">
        <div class="row">
            <div class="col-8">
                <h3 class="my-3 py-3 border-bottom">Split Ticket</h3>
                <h4 class="my-3 py-3">Items</h4>
                <div id="unpaid-items"></div>
                <h4 class="my-3 py-3">Total: <span id="total">0</span></h4>
            </div>
            <div class="mt-3 col-3 offset-1">
                <button id="payCash" class="btn btn-lg btn-block btn-success">Cash</button>
                <button id="payCard" class="btn btn-lg btn-block btn-info">Card</button>
            </div>
        </div>
    </div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="../../scripts/drawAdminMenu.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
<script type="text/javascript">
function fetchOrder(callback) {
    var table = Cookies.get('todo_table_number');
    $.get("../../../api/order", { table_id: table, active: true }, res => {
        if (!res['ok'] || res['result']['items'].length < 1) {
            return callback(null);
        }

        Cookies.set('todo_payment_order_id', res['result']['order_id']);

        if (res['result']['pay_type'] > "1") {
            window.location = "complete";
            return null;
        }

        var unpaid = res['result']['items'].filter(i => i['paid'] != "1");
        return callback(unpaid);
    });
};

function completeOrder() {
    var orderId = Cookies.get('todo_payment_order_id');
    var token = Cookies.get('todo_internal_token');
    $.post('../../../api/order_update', { order_id: orderId, token: token, status: 6 }, res => {
        window.location = "complete"; 
    }).fail(res => {
        console.error(res);
    });
};

function drawOrder(items) {
    if (items.length < 1) {
        return completeOrder();
    }

    for (var i in items) {
        var item = items[i];
        var itemWrapper = $('<div class="form-check"></div>');

        var itemCheckbox = $('<input class="form-check-input" type="checkbox" value="">');
        $(itemCheckbox).attr('id', item['item_id']);
        $(itemCheckbox).data('price', item['price']);
        $(itemCheckbox).change(onChangeSelection);

        var itemLabel = $('<label class="form-check-label"></label>');
        $(itemLabel).attr('for', item['item_id']);
        $(itemLabel).text(item['name'] + ' - ' + item['price']);

        $(itemWrapper).append(itemCheckbox);
        $(itemWrapper).append(itemLabel);
        $('#unpaid-items').append(itemWrapper);
    }
};

function readCheckedItemIds() {
    return $(':checked').map(function () { return this.id; }).get();
};

function onChangeSelection() {
    var checked = readCheckedItemIds();
    var total = 0;

    $.each(checked, (i, id) => {
        total += parseFloat($('#' + id).data('price'));
    });

    $('#total').text(total);
};

function pay(type) {
    var items = readCheckedItemIds();

    if (items.length < 1) {
        alert("Please select at least one item.");
        return;
    }

    var payload = { token: Cookies.get('todo_internal_token'), items: items, payment_type: type };
    $.post("../../../api/payment/pay_items", payload, res => {
        window.location.reload();
    }).fail(res => {
        console.error(res);
    });
};

        $(() => {
            drawAdminMenu();
            fetchOrder(drawOrder);

            $('#payCash').click(() => { pay(2); });
            $('#payCard').click(() => { pay(3); });
        });
    </script>
  </body>
</html>
