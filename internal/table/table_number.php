<?php

if (!isset($_COOKIE['todo_internal_token'])) {
    header("Location: ../login");
    die();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Enter Table Number &ndash; #TODO Eats</title>
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-GxhP7S92hzaDyDJqbdpcHqV5cFflxAx0Yze/X7vUONp43KK1E8eUWaJIMkit3D0R" crossorigin="anonymous">
    </head>
    <body role="document">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
            <div class="container">
                <a class="navbar-brand" href="#">#TODO</a>
            </div>
        </nav>
        <div class="container">
            <div class="mx-auto" style="width:80%">
                <h3 class="mt-2 mb-4 py-2 border-bottom">Table</h3>
                <form id="number-form">
                    <div class="form-group" id="table-number">
                        <label for="table-number"><b>Table number</b></label>
                        <input type="text" class="form-control" placeholder="Number" name="table-number" required>
                    </div>
                    <div class="my-1 py-1"></div>
                    <div id="table-number-submit">
                        <button class="btn btn-primary" type="submit">Confirm</button>
                    </div>
                </form>
            </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
<script type="text/javascript">
// TODO no way to tell who is signed in yet
// (it exists, just not implemented yet)

$(() => {
    $('#number-form').submit(() => {
        var number_input = $('#table-number input').val();

        if (!number_input.match(/^[0-9]{1,2}$/)) {
            alert("Please enter a number.");
            return false;
        }

        var table_number = parseInt(number_input);

        if (isNaN(table_number)) {
            alert("Please enter a number.");
            return false;
        }

        if (table_number < 1 || table_number > 16) {
            alert("Table number must be between 1 and 16.");
            return false;
        };

        Cookies.set('todo_table_number', table_number, { expires: 30, path: '/' });
        window.location = "index";
        return false;
    });
});
    </script>
</html>
