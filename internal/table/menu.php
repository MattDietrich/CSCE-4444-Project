<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="index">#TODO</a>
      </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-10" id="menu"></div>
        </div>
    </div>
    <div class="container-fluid">
      <div class="mt-5" id="spacer"></div>
      <div class="fixed-bottom mb-3 mr-3">
        <div class="row justify-content-end">
          <div class="col-2 offset-10">
            <button class="btn btn-info btn-block">Refill</button>
            <button class="btn btn-danger btn-block">Help</button>
          </div>
        </div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script type="text/javascript">
        $(() => {
            var req = $.ajax({ url: "../../api/menu.php", success: (res) => {
                if (!res['ok'] || !res['result']) {
                    alert("failed to get menu items");
                    return;
                }

                var categories = res['result'];
                for (var catId in categories) {
                    if (categories.hasOwnProperty(catId)) {
                        var category = categories[catId];

                        var categoryHeader = $('<h3 class="mt-4 mb-3 pb-3 border-bottom"></h3>');
                        categoryHeader.text(category['CategoryName']);
                        $('#menu').append(categoryHeader);

                        var categoryContainer = $('<div class="row"></div>');
                        var categoryItems = category['Items'];

                        for (var itemId in categoryItems) {
                            var itemContainer = $('<div class="col-4"></div>');
                            var itemCard = $('<div class="card mb-3"></div>');
                            var itemBody = $('<div class="card-body"></div>');
                            var itemHeader = $('<h5 class="card-title"></h5>');
                            var itemText = $('<p class="card-text"></p>');

                            var item = categoryItems[itemId];
                            $(itemHeader).text(item['Name']);
                            $(itemText).text(item['Price']);

                            $(itemBody).append(itemHeader);
                            $(itemBody).append(itemText);
                            $(itemCard).append(itemBody);
                            $(itemContainer).append(itemCard);
                            $(categoryContainer).append(itemContainer);
                        }
                        $('#menu').append(categoryContainer);
                    }
                }
            }});
        });
    </script>
  </body>
</html>
