function drawAdminMenu() {    
    // select navbar
    var navbar = $('.navbar');

    // create navbar if not exists
    if (!navbar.length) {
        navbar = $('<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5"></nav>');
        $(navbar).append($('<div class="container"><a class="navbar-brand" href="#">#TODO</a></div>'));
        $('body').prepend(navbar);
    }

    // select navbar inner container
    var navbarContainer = $(navbar).find('.container');

    // create navbar container if not exists
    if (!navbarContainer.length) {
        navbarContainer = $('<div class="container"></div>');
        $(navbar).append(navbarContainer);
    }

    // create admin menu
    var adminMenu = $('<ul class="nav navbar-nav navbar-right"></ul>');
    var adminLink = $('<li class="nav-item"><a class="nav-link" href="../login?force=true&target=admin&create_session=false">Admin</li>');

    // add admin menu to navbar
    $(adminMenu).append(adminLink);
    $(navbarContainer).append(adminMenu);
};

