<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="..">#TODO</a>
      </div>
    </nav>

    <div class="container-fluid"></div>
    <div class="container">
        <h1 class="my-3 py-3 border-bottom">Loss Report</h1>
        <ul id="data" class="list-unstyled"></ul>
        <div class="my-2 py-2"><hr /></div>
        <button class="btn btn-info btn-lg" id="btn-print">Print</button>
    </div>
    <div class="container-fluid"></div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script src="../../scripts/drawAdminMenu.js"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script type="text/javascript">
        function drawReport(data) {
            $('#data').append('<li>Number of orders comped: ' + data['num_comped_orders'] + '</li>');
            $('#data').append('<li>Most comped orders: ' + data['comped_orders'][0]['Name'] + 
                ' (' + data['comped_orders'][0]['Occurrences'] + ' comped orders)</li>');
            $('#data').append('<li>Total loss due to comp: ' + data['comp_loss'] + '</li>');
        };

        $(() => {
            drawAdminMenu();
            var token = Cookies.get('todo_internal_token');
            $.get('../../../api/manager_report', { token: token }, res => {
                if (res['result'])
                    drawReport(res['result']);
                else
                    alert("no data");
            });
            $('#btn-print').click(() => { alert("Printing..."); });
        });
    </script>
  </body>
</html>
