<?php
    header("Access-Control-Allow-Methods: GET");

    if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
        header("HTTP/1.1 405 Method Not Allowed");
        header("Allow: GET");
        die();
    }

    if (!isset($_GET['user_id'])) {
        header("Location: edit_select");
        die();
    }

    include_once "../../../api/config/database.php";

    $db = new Database();
    $conn = $db->getConnection();

    $member_stmt = $conn->prepare("SELECT FirstName, LastName, CategoryID, UserID FROM StaffMember WHERE ID = :id");
    $member_stmt->bindValue(':id', $_GET['user_id'], PDO::PARAM_INT);

    if (!$member_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        die();
    }

    $member = $member_stmt->fetch(PDO::FETCH_ASSOC);

    $first_name = $member['FirstName'];
    $last_name = $member['LastName'];
    $category = $member['CategoryID'];

    $user_stmt = $conn->prepare("SELECT UserName FROM SystemUser WHERE ID = :id");
    $user_stmt->bindValue(':id', $member['UserID'], PDO::PARAM_INT);

    if (!$user_stmt->execute()) {
        header("HTTP/1.1 500 Internal Server Error");
        die();
    }

    $user = $user_stmt->fetch(PDO::FETCH_ASSOC);

    $username = $user['UserName'];
?>

<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="..">#TODO</a>
      </div>
    </nav>

    <div class="container-fluid"></div>
    <div class="container">
        <h1 class="mb-4 pb-3 border-bottom">Edit Staff</h1>
        <p class="d-none" id="user-id"><?php echo($_GET['user_id']) ?></p>

        <form id="form">
            <div class="row pb-4 mb-4 border-bottom">
                <div class="col-4">
                    <div class="form-group mx-auto">
                        <label for="first">First Name</label>
                        <input type="text" class="form-control mb-3" id="first" value="<?php echo($first_name) ?>" required>
                        <label for="last">Last Name</label>
                        <input type="text" class="form-control mb-3" id="last" value="<?php echo($last_name) ?>" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group mx-auto"> 
                        <label for="user">Username</label>
                        <input type="text" class="form-control mb-3" id="user" value="<?php echo($username) ?>" readonly>
                        <label for="pass">Password</label>
                        <input type="password" class="form-control mb-3" id="pass" value="        " readonly>
                        <label for="pass-confirm">Confirm Password</label>
                        <input type="password" class="form-control mb-3" id="pass-confirm" value="        " readonly>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group mx-auto">
                        <label for="category">Staff Category</label>
                        <select class="form-control" id="category">
                            <?php 
                                $categories = array("1" => "Wait Staff", "2" => "Kitchen Staff", "3" => "Manager");
                                foreach($categories as $key => $value) {
                                    $this_tag = "<option data-catid='$key'";
                                    if ($key == $category) {
                                        $this_tag .= ' selected';
                                    }
                                    $this_tag .= ">$value</option>";
                                    echo($this_tag);
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-3 offset-9">
                    <button class="btn btn-success btn-lg btn-block" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid"></div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>

<script type="text/javascript">
function formSubmit() {
    var userID = $('#user-id').text();

    var payload = {
        token: Cookies.get('todo_internal_token'),
        id: userID,
        first: $('#first').val(),
        last: $('#last').val(),
        category: $('#category').find(':selected').data('catid')
    };

    $.post('../../../api/edit_staff', payload, res => {
        if (!res['ok']) {
            alert(res['error']);
            return;
        }

        window.location.reload(true);
        return;
    }).fail(res => {
        alert(res['responseJSON']['error']);
    });

    return false;
};

$(() => {

    $('#form').submit(formSubmit);

});
</script>

  </body>
</html>
