<?php

include_once "../../../api/config/database.php";

$db = new Database();
$conn = $db->getConnection();

?>

<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="..">#TODO</a>
      </div>
    </nav>

    <div class="container-fluid"></div>
    <div class="container">
        <h1 class="mb-4 pb-3 border-bottom">Edit Staff</h1>

        <div class="row">
            <div class="col-6 offset-3">
                <form id="form">
                    <label for="user-id">Select Staff Member</label>
                    <select class="form-control" id="user-id">
                    <?php
                        $stmt = $conn->prepare("SELECT ID, FirstName, LastName FROM StaffMember");
                        $stmt->execute();
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                            $id = $row['ID'];
                            $name = $row['FirstName'] . " " . $row['LastName'];
                            echo("<option data-catid='$id'>$name</option>");
                        }
                    ?>
                    </select>
                    <div class="row mt-5 pt-5 border-top">
                        <div class="col-4 offset-8">
                            <button class="btn btn-lg btn-success btn-block" type="submit">Select</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid"></div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>

<script type="text/javascript">
function populateForm() {

};

function formSubmit() {
    var userID = $('#user-id').find(':selected').data('catid');
    window.location = "edit?user_id=" + userID;
    return false;
};

$(() => {
    $('#form').submit(formSubmit);
});
</script>

  </body>
</html>
