<?php

header("Access-Control-Allow-Methods: GET");

if ($_SERVER['REQUEST_METHOD'] !== "GET") {
    header("405 Method Not Allowed");
    header("Allow: GET");
    die();
}

include_once "../../../api/config/database.php";

$db = new Database();
$conn = $db->getConnection();

$stmt = $conn->prepare("SELECT ID, FirstName, LastName FROM StaffMember");
$stmt->execute();

?>

<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="..">#TODO</a>
      </div>
    </nav>

    <div class="container">
        <h1 class="my-3 pb-3 border-bottom">Delete Staff</h1>
        <div class="row mt-2 pt-2">
            <div class="col-6 offset-3">
                <form id="form">
                    <label for="user-id">Select Staff Member</label>
                    <select id="user-id" class="form-control mt-2">
                        <option data-userid="-1" selected>-</option>
                        <?php
                            while ($staff = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                $id = $staff['ID'];
                                $name = $staff['FirstName'] . " " . $staff['LastName'];
                                echo("<option data-userid='$id'>$name</option>");
                            }    
                        ?>
                    </select>
                    <div class="row mt-5 pt-4 border-top">
                        <div class="col-4 offset-8">
                            <button class="btn btn-lg btn-block btn-danger" type="submit">Delete</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid"></div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script type="text/javascript">
        function formSubmit() {
            var userID = $('#user-id').find(':selected').data('userid');

            if (userID < 1) {
                alert("Please select a staff member to delete.");
                return false;
            }

            var name = $('#user-id').find(':selected').val();

            var areYouSure1 = "Are you sure you want to delete " + name + "? This action is NOT REVERSIBLE.";
            var areYouSure2 = "About to delete " + name + " forever. Press Cancel to abort.";

            if (!confirm(areYouSure1) || !confirm(areYouSure2)) {
                return false;
            }

            var payload = {
                token: Cookies.get('todo_internal_token'),
                UserID: userID
            };

            $.post('../../../api/delete_staff', payload, res => {
                if (!res['ok']) {
                    alert(res['error']);
                    return;
                }
                window.location = '..';
            }).fail(res => {
                alert(res['responseJSON']['error']);
                console.error(res);
            });

            return false;
        }

        $(() => {
            $('#form').submit(formSubmit);
        });
    </script>
  </body>
</html>
