<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="..">#TODO</a>
      </div>
    </nav>

    <div class="container-fluid"></div>
    <div class="container">
        <h1 class="mb-4 pb-3 border-bottom">Add Staff</h1>

        <form id="form">
            <div class="row pb-4 mb-4 border-bottom">
                <div class="col-4">
                    <div class="form-group mx-auto">
                        <label for="first">First Name</label>
                        <input type="text" class="form-control mb-3" id="first" required>
                        <label for="last">Last Name</label>
                        <input type="text" class="form-control mb-3" id="last" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group mx-auto"> 
                        <label for="user">Username</label>
                        <input type="text" class="form-control mb-3" id="user" required>
                        <label for="pass">Password</label>
                        <input type="password" class="form-control mb-3" id="pass" required>
                        <label for="pass-confirm">Confirm Password</label>
                        <input type="password" class="form-control mb-3" id="pass-confirm" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group mx-auto">
                        <label for="category">Staff Category</label>
                        <select class="form-control" id="category">
                            <option data-catid="1">Wait Staff</option>
                            <option data-catid="2">Kitchen Staff</option>
                            <option data-catid="3">Manager</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-3 offset-9">
                    <button class="btn btn-success btn-lg btn-block" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid"></div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>

<script type="text/javascript">
function formSubmit() {
    if ($('#pass').val() !== $('#pass-confirm').val()) {
        alert("Password and confirmation do not match.");
        return false;
    }

    var payload = {
        token: Cookies.get('todo_internal_token'),
        user: $('#user').val(),
        pass: $('#pass').val(),
        first: $('#first').val(),
        last: $('#last').val(),
        category: $('#category').find(':selected').data('catid')
    };

    $.post('../../../api/add_staff', payload, res => {
        if (!res['ok']) {
            alert(res['error']);
            return;
        }

        alert('Successfully added ' + payload['first'] + ' ' + payload['last'] + ' to the database.');
        window.location = "..";
        return;
    }).fail(res => {
        alert(res['responseJSON']['error']);
    });
    return false;
};

$(() => {

    $('#form').submit(formSubmit);

});
</script>

  </body>
</html>
