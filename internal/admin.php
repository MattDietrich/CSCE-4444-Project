<?php

if (!isset($_COOKIE['todo_internal_token'])) {
    // not signed in
    header("Location: login?target=admin&create_session=true&force=true");
    die();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin &ndash; #TODO Eats</title>
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-GxhP7S92hzaDyDJqbdpcHqV5cFflxAx0Yze/X7vUONp43KK1E8eUWaJIMkit3D0R" crossorigin="anonymous">
    </head>
    <body role="document">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
            <div class="container">
                <a class="navbar-brand" href="#">#TODO</a>
            </div>
        </nav>
        <div class="container">
            <h2 class="mb-4 pb-4">Admin Menu</h2>
            <ul id="admin-buttons" class="list-unstyled">
                <li class="mt-3 mb-5 py-3">
                    <div class="container">
                        <h4 class="pb-2 mb-3 border-bottom">General</h4>
                        <button id="btnChangeType" class="btn btn-primary btn-lg">Change Device Type</button>
                    </div>
                </li>
                <li class="mt-3 mb-5 py-3">
                    <div class="container">
                        <h4 class="pb-2 mb-3 border-bottom">Table</h4>
                        <a href="table/table_number" class="btn btn-primary btn-lg">Change Table Number</a>
                    </div>
                </li>
                <li class="mt-3 mb-5 py-3">
                    <div class="container">
                        <h4 class="pb-2 mb-3 border-bottom">Danger Zone</h4>
                        <button class="btn btn-danger btn-lg" id="btn-logout">Log Out</button>
                    </div>
                </li>
            </ul>
        </div>
        <div class="my-3 py-3"></div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script type="text/javascript">
        $(() => {
            $('#btnChangeType').click(() => {
                if (Cookies.get('todo_table_number')) {
                    Cookies.remove('todo_table_number', { path: '/' });
                }
                window.location = "selection";
            });
            $('#btn-logout').click(() => {
            $.post('../api/logout', { token: Cookies.get('todo_internal_token') }, res => {
                Cookies.remove('todo_internal_token', { path: '/' });
                window.location = "login";
            });
            });
        });
    </script>
</html>
