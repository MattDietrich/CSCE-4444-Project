<?php

if (!isset($_GET['force']) || !filter_var($_GET['force'], FILTER_VALIDATE_BOOLEAN)) {
    if (isset($_COOKIE['todo_internal_token'])) {
        // login session token cookie exists.
        // TODO validate token
        header("Location: selection"); // automatically sets code to 302 Found and gives the client a redirect location
        die();
    }
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Landing Page &ndash; #TODO Eats</title>
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-GxhP7S92hzaDyDJqbdpcHqV5cFflxAx0Yze/X7vUONp43KK1E8eUWaJIMkit3D0R" crossorigin="anonymous">
    </head>
    <body role="document">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
            <div class="container">
                <a class="navbar-brand" href="#">#TODO</a>
            </div>
        </nav>
        <div class="container">
            <div class="mx-auto" style="width:80%">
                <h2>Internal Login</h2>
                <form id="login-form" action="../api/login.php" method="post">
                    <div class="form-group" id="login-username">
                        <label for="username"><b>Username</b></label>
                        <input type="text" class="form-control" placeholder="Staff login" name="username" required>
                    </div>
                    <div class="form-group" id="login-password">
                        <label for="password"><b>Password</b></label>
                        <input type="password" class="form-control" placeholder="Staff password" name="password" required>
                    </div>
                    <div class="container" id="login-submit">
                        <button class="btn btn-primary" type="submit">Login</button>
                    </div>
                </form>
            </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
<script type="text/javascript">
// TODO no way to tell who is signed in yet
// (it exists, just not implemented yet)

$(() => {
    $('#login-form').submit(() => {
        var create_session = true;

        <?php
            if (isset($_GET['create_session']) && !filter_var($_GET['create_session'], FILTER_VALIDATE_BOOLEAN))
                echo("create_session = false;");
        ?>

        var payload = {
            username: $('#login-username input').val(),
            password: $('#login-password input').val(),
            create_session: create_session
        };

            $.post("../api/login.php", payload, res => {
            if (create_session) {
                var token = res['result']['token'];
                Cookies.set('todo_internal_token', token, { path: '/' });
            }
            window.location = '<?php echo(isset($_GET["target"]) ? $_GET["target"] : "selection"); ?>';
        });

        return false;
    });
});
    </script>
</html>
