<?php

// if not signed in, go to login
if (!isset($_COOKIE['todo_internal_token'])) {
    header("Location: login");
}

// logged in. go to device-type selection
else {
    header("Location: selection");
}

?>
