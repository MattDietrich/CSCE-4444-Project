<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="index">#TODO</a>
      </div>
    </nav>
	<div class="container">
      <h1 class="border-bottom pb-3 mb-3">Ready Take Out Orders</h1>
      <div class="row" id="orders"></div>
    </div>
    <div class="container-fluid"></div>
	<div class="container">
		<form id="orderId-form">
			<div class="form-group">
				<label for="orderNum"><b>Enter Order to Update :</b></label>
				<input type="text" class="form-control" placeholder="Order Id" name="orderNum" id="orderNum" required>
			</div>
			<div class="my-1 py-1"></div>
			<div id="order-number-submit">
				<button class="btn btn-primary" type="submit">Confirm</button>
			</div>
		</form>
	</div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script type="text/javascript">
var ORDER_STATUS_TEXT = {
    "1": "Order placed",
    "2": "Order cooking",
    "3": "Order ready for delivery",
    "4": "Awaiting payment",
    "5": "Order cancelled"
};

function fetchPendingOrders(callback) {
	var result_orders = [];
	$.get("../../api/order.php", { status : 3 }, res => {
		var orders = res['result']['orders'];
		for (var i in orders) {
			result_orders.push(orders[i]);
		}
		return callback(result_orders);
	});
};

function drawBadges(comments, allergy) {
	var result = $('<ul class="card-text list-unstyled list-inline"></ul>');
	if (comments && comments !== "")
		$(result).append('<li class="list-inline-item"><span class="badge badge-info">Comment</span></li>');
	if (allergy && allergy !== "")
		$(result).append('<li class="list-inline-item"><span class="badge badge-danger">Allergy</span></li>');
	return result;
};

function drawOrders(orders) {
	$('#orders').empty();

	for (var i in orders) {
		var order = orders[i];
		if (!order) continue;
		
		var table_number = parseInt(order['table'])
		if(table_number == -1) {
			var orderElement = $('<div class="col-4"></div>');
			var orderCard = $('<div class="card order-card" id="' + order['order_id'] + '"></div>');
			var orderCardBody = $('<div class="card-body"></div>');
			var orderCardTitle = $('<h5 class="card-title">#' + order['order_id'] + '</h5>');
			var orderCardList = $('<ul class="card-text"></ul>');

			var items = order['items'];
			for (var item_i in items) {
				var item = items[item_i];
				var item_li = $('<li>' + item['name'] + '</li>');
				$(orderCardList).append(item_li);
			}

			// draw body
			$(orderCardBody).append(orderCardTitle);
			$(orderCardBody).append('<p class="font-weight-bold">Take Out</p>');
			$(orderCardBody).append(orderCardList);
			$(orderCardBody).append(drawBadges(order['comments'], order['allergy']));

			$(orderCard).append(orderCardBody);
			$(orderElement).append(orderCard);
			$('#orders').append(orderElement);
		}
	}
};
		
function fetchAndDraw() {
	fetchPendingOrders(drawOrders);
};

$(() => {
	fetchAndDraw();
    setInterval(fetchAndDraw, 5000);
	
	$('#orderId-form').submit(() => {
		var orderNumber = $('#orderNum').val();
        orderNumber = parseInt(orderNumber);

        var tableToken = Cookies.get('todo_internal_token');

        var payload = {
            token: Cookies.get('todo_internal_token'),
            order_id: orderNumber,
            status: 6
        };

        console.log(payload);

        $.post("../../api/order_update.php", payload, res => {
            if (!res['ok']) alert(res['error']);
        }).fail(console.error);
	});
});
    </script>
  </body>
</html>
