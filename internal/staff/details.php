<?php

// ensure that a table number is given.
// if no table number is given, 400
// if table number is not a number, 400
// if table number is outside [1-16], 400
// else 200 and continue


/** set response header to 400 Bad Request, echo error page, and die */
function die_with_400() {
    header("HTTP/1.1 400 Bad Request");
    echo(file_get_contents("../error/400.html"));
    die();
}


// ensure something is given
if (!isset($_GET['table']))
    die_with_400();


// ensure value is an integer or a numeric string
$table = $_GET['table'];
if (!is_int($table) && !is_numeric($table))
    die_with_400();


// ensure value is within acceptable range
$table = intval($table);
if ($table < 1 || $table > 16)
    die_with_400();

// guess we're OK. carry on.

?>

<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        var TABLE_NUMBER = "<?php echo($_GET['table']); ?>";
        if (TABLE_NUMBER === "") {
            alert("No table number specified.");
        }
        else {
            TABLE_NUMBER = parseInt(TABLE_NUMBER);
        }
    </script>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="index">#TODO</a>
      </div>
    </nav>
    <div class="container-fluid"></div>
    <div class="container">
        <h2 class="mb-5 pb-4 border-bottom">Details: Table <?php echo($_GET['table']) ?></h2>

        <div class="row">
            <div class="col-8">
                <h4 class="my-3" id="h4-name" style="display:none">Table name: <span id="table-name"></span></h4>
                <h4 class="my-3" id="h4-order" style="display:none">Order ID: <span id="order-id"></span></h4>
                <h4 class="my-3">Order status: <span id="order-status">...</span></h4>
                <h4 class="my-3">Needs refill: <span id="needs-refill">...</span></h4>
                <h4 class="my-3">Needs help: <span id="needs-help">...</span></h4>
                <h4 class="mt-5">Items:</h4>
                <ul class="list-unstyled" id="items"></ul>
                <h4 class="my-5">Total: <span id="order-total"></span></h4>
                <button class="btn btn-info btn-lg my-3" id="btn-deliver" style="display:none;">Deliver Order</button>
            </div>
            <div class="col-4">
                <button class="btn btn-info btn-block btn-lg my-3" id="btn-clear">Clear Requests</button>
                <button class="btn btn-info btn-block btn-lg my-3" id="btn-clear-name">Clear Name</button>
                <div id="btns-spacer" class="py-1"><hr /></div>
                <button class="btn btn-success btn-block btn-lg my-3" id="btn-cash">Cash Out</button>
                <button class="btn btn-danger btn-block btn-lg my-3" id="btn-comp">Comp Order</button>
                <button class="btn btn-danger btn-block btn-lg my-3" id="btn-cancel">Cancel Order</button>
            </div>
        </div>
    </div>
    <div class="container-fluid"></div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script src="../scripts/drawAdminMenu.js"></script>
    <script type="text/javascript">
        var ORDER_TEXT = {
            "1": "Placed",
            "2": "Cooking",
            "3": "Ready for Delivery",
            "4": "Awaiting Payment",
            "5": "Cancelled"
        };

        function fetchOrder(callback) {
            var url = "../../api/order.php?table_id=" + TABLE_NUMBER + "&active=true";
            $.get(url, callback);
        };

        function fetchTableStatus(callback) {
            var url = "../../api/table/read?id=" + TABLE_NUMBER;
            $.get(url, callback);
        };

        function drawEverything() {
            drawOrderStatus();
            drawTableStatus();
        };

        function drawOrderStatus() {
            fetchOrder(res => {
                if (!res['ok'] || !res['result'] || !res['result']['status']) {
                    $('h4 span#order-status').text('None');
                    $('h4 span#order-total').text('-');
                    $('ul#items').empty().append('<li>None</li>');
                }

                if (!res['result'])
                    return;

                var order = res['result'];

                if (order.hasOwnProperty('order_id')) {
                    $('h4 span#order-id').text(order['order_id']);
                    $('h4#h4-order').show();
                } else {
                    $('h4 span#order-id').text("none");
                    $('h4#h4-order').hide();
                }

                if (order.hasOwnProperty('status')) {
                    $('h4 span#order-status').text(ORDER_TEXT[order['status']]);
                    if (order['status'] === "3") {
                        $('#btn-deliver').show();
                    } else {
                        $('#btn-deliver').hide();
                    }
                }

                if (order.hasOwnProperty('status') && order.hasOwnProperty('total_price')) {
                    $('#order-total').text(order['total_price']);
                }

                if (order['items']) {
                    $('ul#items').empty();
                    for (var item_i in order['items']) {
                        var item = order['items'][item_i];
                        var item_li = $('<li></li>').append(item['name'] + " - " + item['price']);
                        if (item['comped']) item_li.append(' (COMPED)');
                        $('ul#items').append(item_li);
                    }
                }
            });
        };

        function drawTableStatus() {
            fetchTableStatus(res => {
                var table = res['table'];

                if (!res['ok'] || !table) {
                    $('#needs-*').text('Error');
                }

                else {
                    $('#needs-refill').text((table['needs_refill'] == "1" ? "YES" : "NO"));
                    $('#needs-help').text((table['needs_help'] == "1" ? "YES" : "NO"));
                }
            });
            $.get("../../api/table_name", { token: Cookies.get('todo_internal_token'), tableID: TABLE_NUMBER }, res => {
                if (res['result']) {
                    $('#table-name').text(res['result']);
                    $('#h4-name').show();
                }
            });
        };

        function clearStatus() {
            var really = confirm("Clear refill and help status for table " + TABLE_NUMBER + "?");
            if (really) {
                var token = Cookies.get('todo_internal_token', { path: '/' });
                var payload = { table_id: TABLE_NUMBER, needs_refill: false, needs_help: false, token: token };
                $.post("../../api/table/update", payload, () => {
                    window.location.reload();
                }).fail((res) => {
                    console.log(res);
                });
            }
        };

        function cancelOrder() {
            if ($('h4 span#order-id').text() === "none" || !$('h4#h4-order').is(':visible')) {
                alert("Table " + TABLE_NUMBER + " has no order to cancel.");
                return;
            }

            var really = confirm("Cancel order for table " + TABLE_NUMBER + "?");
            if (really) {
                var token = Cookies.get('todo_internal_token', { path: '/' });
                var payload = { order_id: $('#order-id').text(), status: 5, token: token };
                $.post("../../api/order_update", payload, () => {
                    window.location = '.';
                }).fail((res) => {
                    console.log(res);
                });
            }
        };

        function compOrder() {
            if ($('h4 span#order-id').text() === "none" || !$('h4#h4-order').is(':visible')) {
                alert("Table " + TABLE_NUMBER + " has no order to comp.");
                return;
            }

            var really = confirm("Comp order for table " + TABLE_NUMBER + "?");
            if (really) {
                var token = Cookies.get('todo_internal_token', { path: '/' });
                var payload = { order_id: $('#order-id').text(), token: token };
                $.post('../../api/payment/comp_order', payload, res => {
                    window.location = '.';
                }).fail(res => { console.error(res); });
            };
        };

        function clearName() {
            $.post("../../api/table_name", { token: Cookies.get('todo_internal_token'), tableID: TABLE_NUMBER }, res => {
                window.location.reload();
            });
        };

        function deliverOrder() {
            if (!$('#btn-deliver').is(":visible")) return;

            var token = Cookies.get('todo_internal_token', { path: '/' });
            var payload = { order_id: $('#order-id').text() , status: 4 , token: token };
            $.post("../../api/order_update", payload, () => {
                window.location.reload();
            }).fail(console.log);
        };

        function cashOutOrder() {
            if ($('#order-status').text().toUpperCase() == "NONE") return;
            if (!confirm("Cash out table " + $('#order-id').text() + '?')) return;

            $.post("../../api/payment/pay_order", {
                token: Cookies.get('todo_internal_token'),
                order_id: $('#order-id').text(),
                payment_type: 2
            }, res => {
                window.location = '.';
            }).fail(console.error);
        };

        $(() => {
            drawAdminMenu();
            drawEverything();
            setInterval(drawEverything, 5000);

            $('#btn-clear').click(clearStatus);
            $('#btn-clear-name').click(clearName);
            $('#btn-cancel').click(cancelOrder);
            $('#btn-comp').click(compOrder);
            $('#btn-deliver').click(deliverOrder);
            $('#btn-cash').click(cashOutOrder);
        });
    </script>
  </body>
</html>
