<!DOCTYPE html>
<html>
  <head>
    <script type="text/javascript">const ORDER_ID = <?php echo $_GET['id']; ?>;</script>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="index">#TODO</a>
      </div>
    </nav>
    <div class="container-fluid"></div>
    <div class="container">
        <h1 class="border-bottom pb-3 mb-3">Order <span id="order-id"></span> - Table <span id="table-number"</span></h1>
        <div class="my-2 py-2">
            <h3>Items</h3>
            <ul id="items"></ul>
        </div>
        <div class="my-2 py-2">
            <h3>Comments</h3>
            <p id="comments"></p>
        </div>
        <div class="my-2 py-2">
            <h3>Allergy info</h3>
            <p id="allergy"></p>
        </div>
      <div class="row mt-2 py-4 border-top">
        <div class="col-4">
            <button class="btn btn-primary" style="display:none;" id="btnBegin">Begin</button>
            <button class="btn btn-success" style="display:none;" id="btnComplete">Complete</button>
      </div>
    </div>
    <div class="container-fluid"></div>
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script src="//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script type="text/javascript">
        function getToken() {
            return Cookies.get("todo_internal_token", { path: "/" });
        };

        function fetchOrder(callback) {
            var orderId = ORDER_ID;
            var token = getToken();


            $.get("../../api/order", { order_id: orderId, token: token }, res => {
                var result = null;

                if (res['ok'])
                    result = res['result'];

                if (res['result']['status'] === "1") {
                    $('#btnBegin').show(0);
                } else if (res['result']['status'] == "2") {
                    $('#btnComplete').show(0);
                }

                return callback(result);
            }).fail(res => { alert("Failed to get info for order: " + orderId); });
        };

        function fetchAndDraw() {
            fetchOrder(drawOrder);
        };

        function drawOrder(order) {
            $('#order-id').text(order['order_id']);
            $('#table-number').text(order['table']);
            $('#comments').text(order['comments'] || "None");
            $('#allergy').text(order['allergy'] || "None");

            for (var i in order['items']) {
                $('#items').append('<li>' + order['items'][i]['name'] + '</li>');
            }
        };

        function startOrder() {
            var token = getToken();
            console.log("token: " + JSON.stringify(token));
            $.post("../../api/order_update", { order_id: ORDER_ID, status: 2, token: token }, res => {
                window.location.reload(true);
            });
        };

        function completeOrder() {
            var token = getToken();
            console.log("token: " + JSON.stringify(token));
            $.post("../../api/order_update", { order_id: ORDER_ID, status: 3, token: token }, res => {
                window.location = "index";
            });
        };

        $(() => {
            fetchAndDraw();

            $('#btnBegin').click(startOrder);
            $('#btnComplete').click(completeOrder);
        });
    </script>
  </body>
</html>
