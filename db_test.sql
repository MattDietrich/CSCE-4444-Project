INSERT INTO SystemUser (ID, Username, Password) VALUES (1, 'mary_contrary', 'password');
INSERT INTO StaffMember (ID, Firstname, LastName, CategoryID, UserID) VALUES (1, 'Marylou', 'McCloskey', 1, 1);

INSERT INTO SystemUser (ID, Username, Password) VALUES (2, 'dc_comix', 'notMYpassword');
INSERT INTO StaffMember (ID, Firstname, LastName, CategoryID, UserID) VALUES (2, 'Darius', 'Clay', 1, 2);

INSERT INTO SystemUser (ID, Username, Password) VALUES (3, 'kittens', 'puppies');
INSERT INTO StaffMember (ID, Firstname, LastName, CategoryID, UserID) VALUES (3, 'Elicia', 'Lawless', 1, 3);

INSERT INTO SystemUser (ID, Username, Password) VALUES (4, 'coffee_dad_92', '12345');
INSERT INTO StaffMember (ID, Firstname, LastName, CategoryID, UserID) VALUES (4, 'Kristofer', 'Thurston', 1, 4);

INSERT INTO SystemUser (ID, Username, Password) VALUES (5, 'tess_tube', 'go_away_mom');
INSERT INTO StaffMember (ID, Firstname, LastName, CategoryID, UserID) VALUES (5, 'Tessa', 'Lomax', 2, 5);

INSERT INTO SystemUser (ID, Username, Password) VALUES (6, 'skateboardGOD', 'imNotSadToday0404');
INSERT INTO StaffMember (ID, Firstname, LastName, CategoryID, UserID) VALUES (6, 'Shane', 'Stump', 2, 6);

INSERT INTO SystemUser (ID, Username, Password) VALUES (7, 'dbo1979', 'bday040379');
INSERT INTO StaffMember (ID, Firstname, LastName, CategoryID, UserID) VALUES (7, 'Deon', 'Otis', 2, 7);

INSERT INTO SystemUser (ID, Username, Password) VALUES (8, 'rb0001', '$0ftw4r3__');
INSERT INTO StaffMember (ID, Firstname, LastName, CategoryID, UserID) VALUES (8, 'Renee', 'Bryce', 3, 8);


/**************************************************************
- Create customers and corresponding user entries
**************************************************************/
INSERT INTO SystemUser (ID, Username, Password) VALUES (9, 'sbarro33', 'a');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (1, 'Stephany', 'Barrows');

INSERT INTO SystemUser (ID, Username, Password) VALUES (10, 'fireball8', 'wh1$k3y.$0ur$');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (2, 'Linnea', 'Binkley');

INSERT INTO SystemUser (ID, Username, Password) VALUES (11, 'guitario', 'soundofSALESMEN');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (3, 'Elliott', 'Littlefield');

INSERT INTO Customer (ID, FirstName, LastName) VALUES (4, 'Claris', 'Dunham');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (5, 'Bo', 'Simms');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (6, 'Jocelyn', 'Calabrese');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (7, 'Tommy', 'Dunbar');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (8, 'Eileen', 'Blevins');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (9, 'Shalonda', 'Winn');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (11, 'Elvin', 'Raney');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (12, 'Janay', 'Zaragoza');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (13, 'Francisco', 'Medrano');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (14, 'Cynthia', 'French');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (15, 'Wynona', 'Branham');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (16, 'Charlotte', 'Dunaway');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (17, 'Loreta', 'Suarez');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (18, 'Markus', 'Fenton');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (19, 'Fred', 'Leblanc');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (20, 'Mickey', 'McCutcheon');
INSERT INTO Customer (ID, FirstName, LastName) VALUES (21, 'Paige', 'Crane');


/* Menu items 
Category ID
1- Entree
2- Appetizer
3- Sides
4- Dessert
5- Kid's Meal
6- Beverage
*/

INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (1, 'Hamburger', 5.95, 1, '/~team/dev/foodImages/hamburger.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (2, 'Fried pickles', 3.50, 2, '/~team/dev/foodImages/fried_pickes.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (3, 'French fries', 1.95, 3, '/~team/dev/foodImages/french_fries.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (4, 'Ice cream', 2.95, 4, '/~team/dev/foodImages/ice_cream.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (5, 'Junior hamburger combo', 4.95, 5, '/~team/dev/foodImages/hamburger_kids_combo.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (6, 'Hot Dog', 4.00, 1, '/~team/dev/foodImages/hot-dog-pic.png');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (7, 'Personal Pizza', 4.50, 1, '/~team/dev/foodImages/Personal_Pizza.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (8, 'Fried Okra', 2.10, 3, '/~team/dev/foodImages/okra.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (9, 'Onion Rings', 2.25, 3, '/~team/dev/foodImages/onion-rings1.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (10, 'Water', 0.00, 6, '/~team/dev/foodImages/waterglass.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (11, 'Soda', 1.50, 6, '/~team/dev/foodImages/soda.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (12, 'Chocolate Cake', 3.50, 4, '/~team/dev/foodImages/cake.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (13, 'Pecan Pie', 3.30, 4,'/~team/dev/foodImages/pecan_pie.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (14, 'Buffalo Wings', 3.50, 2,'/~team/dev/foodImages/wings.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (15, 'Kid\'s Chicken Nuggets', 3.50, 5, '/~team/dev/foodImages/nuggets.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (16, 'Chicken Tenders', 4.25, 1, '/~team/dev/foodImages/chicken-tenders.png');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (17, 'Lemonade', 1.75, 6, '/~team/dev/foodImages/lemonade.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (18, 'Coffee', 1.50, 6, '/~team/dev/foodImages/coffee.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (19, 'Tea', 3.50, 6, '/~team/dev/foodImages/Tea.png');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (20, 'Sliders', 3.25, 2, '/~team/dev/foodImages/sliders.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (21, 'Nachos', 2.75, 2, '/~team/dev/foodImages/nachos.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (22, 'Chili', 2.30, 3, '/~team/dev/foodImages/chili.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (23, 'Pudding', 1.25, 4, '/~team/dev/foodImages/pudding.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (24, 'Pancakes', 4.00, 5,'/~team/dev/foodImages/pancakes.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (25, 'Chicken Noodle Soup', 2.75, 2, '/~team/dev/foodImages/soup.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (26, 'Cheese Sticks', 2.50, 3, '/~team/dev/foodImages/cheesesticks.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (27, 'Chicken-Fried Steak', 6.50, 1, '/~team/dev/foodImages/chicken-fried-steak.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (28, 'Cinnamon Rolls', 3.00, 4, '/~team/dev/foodImages/cinnamon-rolls.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (29, 'PBJ Sandwich', 2.50, 5, '/~team/dev/foodImages/PBJ-Sandwich.jpg');
INSERT INTO MenuItem (ID, Name, Price, CategoryID, ImageURL) VALUES (30, 'Pizza Slice', 3.00, 5, '/~team/dev/foodImages/pizza-slice.jpg');


/* RestaurantOrder test cases*/

INSERT INTO RestaurantOrder (ID, FromTableId, AssignedStaffID,Status,Paid,Comments) VALUES (1,1,2,1,false,'Soy Allergy');

/*Table name test case*/
UPDATE RestaurantTable 
    SET Name = 'Kekistan' 
    WHERE ID = 1; 
	
/*Coupon test case*/

INSERT INTO Coupons (ID, TypeID, Code, ExpDay,ExpMonth, ExpYear) VALUES (1, 1, 12345, 4,20,2019);	
