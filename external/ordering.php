<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
    <link rel="stylesheet" href="/mockups/static/table.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="index">#TODO</a>
      </div>
    </nav>
    <div class="container-fluid"></div>
    <div class="container">
      <div class="row">
        <div class="col-9" id="items-wrapper" style="display:none">
            <h2 class="my-3 py-3">Confirm your order</h2>
            <div id="items-wrapper-inner"></div>
            <div class="my-4 py-3">
                <div class="form-group" id="order-input-group">
                    <label for="comments">Comments</label>
                    <textarea class="form-control mb-5" id="comments" rows="3"></textarea>
                    <label for="allergy">Allergy Info</label>
                    <textarea class="form-control" id="allergy" rows="3"></textarea>
                </div>
            </div>
        </div>
        <div class="col-9" id="new-order-wrapper" style="display:none">
            <h2>No order yet!</h2>
            <p>Tap the button on the right to start your order.</p>
        </div>
        <div class="col-3" id="buttons-wrapper" class="justify-content-end">
            <!-- if orders.length > 0 -->
            <div id="btns-existing-order" style="display:none">
                <button class="btn btn-lg btn-block btn-info" id="btnAddItems">Another Order</button>
                <button class="btn btn-lg btn-block btn-success" id="btnPay">Pay</button>
            </div>
            <div id="btns-new-order" style="display:none">
                <a class="btn btn-lg btn-success" href="add_items">Start Order</a>
            </div>
            <div class="my-2 py-2"><hr /></div>
            <div id="btns-danger">
                <button class="btn btn-lg btn-danger btn-block" id="btnCancelOrder">Cancel Order</button>
            </div>
        </div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script type="text/javascript">
function drawOrderItems() {
    var order = readOrder();

    console.log(order);

    if (order['orders'].length > 0) {
        for (var order_i in order['orders']) {
            var order_num = parseInt(order_i) + 1;
            console.log('order ' + order_num);

            var this_order = order['orders'][order_i];

            var order_wrapper = $('<div id="order-' + order_num + '"></div>');

            var order_header = $('<h3 class="py-2 my-2 border-bottom">Order ' + order_num + '</h3>');
            $(order_wrapper).append(order_header);

            var order_items = $('<ul class="list-unstyled"></ul>');

            for (var item_i in this_order) {
                var this_item = this_order[item_i];
                $(order_items).append($('<li>' + this_item['description'] + '</li>'));
            }

            $(order_wrapper).append(order_items);
            $('#items-wrapper-inner').append(order_wrapper);
        }

        $('#comments').val(order['comments']).trigger('change');
        $('#allergy').val(order['allergy']).trigger('change');

        $('#items-wrapper').show();
        $('#btns-existing-order').show();
    } else {
        $('#new-order-wrapper').show();
        $('#btns-new-order').show();
    }
};

function readOrder() {
    var order = Cookies.get('todo_temp_order');
    return order ? JSON.parse(order) : { orders: [], comments: "", allergy: "" };
};

function saveOrder(overrideInputs) {
    var order = readOrder();

    if (overrideInputs) {
        order['comments'] = $('#comments').val();
        console.log('comments value is:', $('#comments').val());

        order['allergy'] = $('#allergy').val();
        console.log('allergy value is:', $('#allergy').val());
    }

    Cookies.set('todo_temp_order', JSON.stringify(order));
};

function submitOrder() {
    saveOrder(true);

    var order = readOrder();

    if (order['orders'].length < 1) {
        alert("Can't submit an order that doesn't exist!");
        return;
    }

    var items = [];

    for (var order_i in order['orders']) {
        var this_order = order['orders'][order_i];
        for (var item_i in this_order) {
            var this_item = this_order[item_i];
            items.push(this_item['id']);
        }
    }

    var payload = {
        table_id: -1,
        items: items,
        comments: order['comments'],
        allergy: order['allergy']
    };

    $.post("../api/order.php", payload, res => {
        var order_id = res['result']['order_id'];
        Cookies.remove('todo_temp_order');
        window.location = "index";
    }).fail(res => {
        console.log(JSON.stringify(res, null, 4));
        alert(res['responseJSON']['error']);
    });
};

function cancelOrder() {
    var really = confirm("Cancel order?");
    if (!really) return;

    Cookies.remove('todo_temp_order');
    window.location = 'index';
};

function anotherOrder() {
    saveOrder(true);

    var order = readOrder();
    if (order['orders'].length < 4) {
        window.location = "add_items";
    } else {
        alert("Only four orders per table");
    }
};

function pay() {
	var order = readOrder();
	Cookies.set('todo_temp_order', JSON.stringify(order));
	window.location = "payment";
}

$(() => {
    drawOrderItems();
    $('#btnPay').click(pay);
    $('#btnCancelOrder').click(cancelOrder);
    $('#btnAddItems').click(anotherOrder);
});
    </script>
  </body>
</html>
