<!DOCTYPE html>
<html>
  <head>
    <title>#TODO Eats</title>
    <link rel="shortcut icon" href="/mockups/static/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-XI0PIujkSZEzZ5m8WTEm+krRuIpfO+vHrEznaIrhTAkbrbIvfs11kzCqUarPR7yn" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/mockups/static/custom_styles.css" rel="stylesheet">
    <link rel="stylesheet" href="/mockups/static/table.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
      <div class="container">
        <a class="navbar-brand" href="index">#TODO</a>
      </div>
    </nav>
    <div class="container-fluid"></div>
    <div class="container">
      <div class="row">
        <div class="col-10" id="menu-wrapper">
            <!-- menu items here -->
        </div>
      </div>
        <button class="btn btn-success" id="btn-order">Order</button>
        <div class="my-5 py-5"></div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <script src="/mockups/static/scripts/enums.js"></script>
    <script type="text/javascript">
function drawOrderScreen() {
    $.get("../api/menu.php", res => {
        categories = res['result'];
        for (var cId in categories) {
            category = categories[cId];

            var categoryContainer = $('<div class="radio" id="category-'+cId+'"></div>');
            $(categoryContainer).append('<h3 class="py-2 my-2 border-bottom">' + category['CategoryName'] + '</h3>');

            var noneElement = $('<div class="radio"></div>');
            var noneLabel = $('<label></label>');
            var noneRadio = $('<input type="radio" name="category-' + cId + '" id="-1" checked="checked">');

            $(noneLabel).append(noneRadio);
            $(noneLabel).append(' None');
            $(noneElement).append(noneLabel);
            $(categoryContainer).append(noneElement);
            $(categoryContainer).append('<br />');

            items = category['Items'];
            for (var itemIndex in items) {
                var item = items[itemIndex];

                var itemElement = $('<div class="radio"></div>');
                var itemLabel = $('<label></label>');
                var itemRadio = $('<input type="radio" name="category-'+ cId +'" id="'+ item['ID'] +'">');

                $(itemLabel).append(itemRadio);
                $(itemLabel).append(' ' + item['Name'] + ' - ' + item['Price']);
                $(itemElement).append(itemLabel);

                $(categoryContainer).append(itemElement);
                $(categoryContainer).append('<br />');
            }

            $('#menu-wrapper').append(categoryContainer);
        }
    });
};

function submitOrder() {
    var items = [];

    $('#menu-wrapper input[type=radio]:checked').each((i, obj) => {
        var itemId = $(obj).attr('id');
        var itemDesc = $(obj).parent().text();

        if (itemId > 0) {
            items.push({ id: itemId, description: itemDesc });
        }
    });

    if (items.length > 0) {
        var raw = Cookies.get('todo_temp_order');
        var current_order = raw ? JSON.parse(raw) : { orders: [], comments: "", allergy: "" };
        current_order['orders'].push(items);
        Cookies.set('todo_temp_order', JSON.stringify(current_order));
    }

    window.location = 'ordering';
}

$(() => {
    drawOrderScreen();
    $('#btn-order').click(submitOrder);
});
    </script>
  </body>
</html>
