DO NOT push directly to master, dev, front-end, or back-end. Instead, follow the steps below:

1. Navigate to your parent branch (front-end or back-end):    
    $ git checkout [parent branch name]

2. Create a new branch for the issue you're working on:    
    $ git checkout -b issueN    
    (Replace N with the issue number)

3. When you have finished working on the issue, commit and push to the issue branch    
    $ git commit -m "[descriptive commit message]"    
    $ git push -u origin issueN

4. On gitlab, create a merge request    
    Source branch: issueN    
    Target branch: [parent branch name]